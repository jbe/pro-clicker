import React, {Component} from 'react';
import { Icon } from "@blueprintjs/core";
/* import unitTypes from "data/unitTypes";*/
import flagHelp from "data/flagHelp";
/* import {humanizeSecs} from "utils";*/

export default class FlagEffectRow extends Component {
  render() {
    const {
      flag
    } = this.props;

    const entry = flagHelp.get(flag)

    const {
      help
    } = entry ? entry.toJS() : {};

    const tdStyle = {paddingLeft: "1em"};


    return (
      <tr>
        <td>
          <Icon icon={"tick"} title={flag}/>
        </td>
        <td style={{...tdStyle}}></td>
        <td>
          { help || flag }
        </td>
      </tr>
    );
  }
}
