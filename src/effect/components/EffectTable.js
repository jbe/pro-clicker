import React, {Component} from 'react';
import { Icon } from "@blueprintjs/core";
import UnitCountRow from "unit/components/UnitCountRow";
import FlagEffectRow from "effect/components/FlagEffectRow";
import governmentTypes from "data/governmentTypes";

export default class EffectTable extends Component {
  render() {
    const {
      effect
    } = this.props;

    if (!effect || !effect.size) return (
      <p>Nothing.</p>
    );

    const tdStyle = {paddingLeft: "1em"};

    const units = effect.get("units");
    const unitEffectJsx = units && units.map((count, unitId) =>
      <UnitCountRow
        key={unitId}
        unitId={unitId}
        count={count}
      />
    ).toArray()

    const flags = effect.get("flags");
    const flagEffectJsx = flags && flags.map(flag =>
      <FlagEffectRow key={flag} flag={flag}/>
    );

    const setGovernment = effect.get("set_government");
    const setGovernmentEffectJsx = setGovernment && (
      <tr>
        <td>
          <Icon icon={"take-action"}/>
        </td>
        <td style={{...tdStyle}}></td>
        <td>
          Your company adopts {governmentTypes.getIn([setGovernment, "header"])}.
        </td>
      </tr>
    );

    return (
      <div className="EffectTable">
        <table>
          <tbody>
            {unitEffectJsx}
            {flagEffectJsx}
            {setGovernmentEffectJsx}
          </tbody>
        </table>
      </div>
    );
  }
}
