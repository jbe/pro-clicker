import { Seq } from "immutable";
import humanizeDuration from "humanize-duration";

// UI helpers

export function humanizeSecs(seconds) {
  return humanizeDuration(Math.floor(seconds) * 1000);
}

export function intersperse(arr, sep) {
  if (arr.length === 0) {
    return [];
  }

  return arr.slice(1).reduce(function(xs, x, i) {
    return xs.concat([sep, x]);
  }, [arr[0]]);
}

// HTML API helpers

export function download(filename, text) {
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);

  if (document.createEvent) {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  }
  else {
    pom.click();
  }
}

// Data

export function serialize(data) {
  return btoa(JSON.stringify(data));
}

export function deserialize(str) {
  return JSON.parse(atob(str));
}

// Immutable

export function fromJSOrdered(js) {
    return typeof js !== 'object' || js === null ? js :
        Array.isArray(js) ?
        Seq(js).map(fromJSOrdered).toList() :
        Seq(js).map(fromJSOrdered).toOrderedMap();
}

const add = (a, b) => a + b;

export const addMapValues = (a, b) => a.mergeWith(add, b);

export function sumMapValues(maps) {
  return maps.reduce(addMapValues);
}
