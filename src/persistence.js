import { fromJS } from "immutable";
import {
  download,
  serialize,
  deserialize
} from "utils";
import {
  saveSlotLocalStorageKey,
  autoSaveSlotKey,
  initialAutoSaveSlot,
} from "data/constants";
import patchSave from "patchSave";

const rememberedAutoSaveSlot = localStorage.getItem(autoSaveSlotKey);
export const rememberedOrInitialAutoSaveSlot = rememberedAutoSaveSlot ? JSON.parse(rememberedAutoSaveSlot) : initialAutoSaveSlot;

export function readSave(slotNumber) {
  /* console.log("Load:", {slotNumber});*/
  const str = localStorage.getItem(saveSlotLocalStorageKey + slotNumber);
  return str ? patchSave(fromJS(deserialize(str))) : null;
}

export function readSaveName(slot) {
  return localStorage.getItem(saveSlotLocalStorageKey + slot + "_name");
}

export function writeSave(world, slot, slotName) {
  /* console.log("Save:", {slot, slotName});*/
  const str = serialize(world.toJS());
  localStorage.setItem(saveSlotLocalStorageKey + slot, str);
  localStorage.setItem(saveSlotLocalStorageKey + slot + "_name", slotName);
}

export function downloadSave(world, name) {
  download(name, serialize(world));
}
