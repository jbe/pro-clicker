import React, {Component} from 'react';
import { Card, Elevation, Icon } from "@blueprintjs/core";

export default class Panel extends Component {
  render() {
    const {
      children,
      className,
      title,
      maxWidth,
      width,
      icon
    } = this.props;

    return (
      <Card className={className} style={{display: "inline-block", verticalAlign: "top", margin: "1rem"}} elevation={Elevation.TWO}>
        <div style={{width, maxWidth}}>
          {
            title && (
              <h5>
                {icon && <Icon icon={icon}/>} {title}
              </h5>
            )
          }
          {children}
        </div>
      </Card>
    );
  }
}
