import React, { Component } from 'react';

class ProClickerIntroSong extends Component {

  constructor(props) {
    super(props);

    this.audioRef = React.createRef();
  }

  componentDidMount() {
    const audio = this.audioRef.current;
    audio.play();
    audio.currentTime = 29;
  }

  render() {
    return <audio ref={this.audioRef}>
      <source src="/canyon.mid.mp3" type="audio/mpeg" />
    </audio>;
  }
}

class IntroPage extends Component {

  state = {
    hasFocused: false
  };

  render() {
    const {
      onClose
    } = this.props;

    const {
      hasFocused
    } = this.state;

    // We need the user to interact with the page before we're allowed to play audio.
    if (!hasFocused) {
      return <button
        style={{ fontSize: "2em", margin: "2em 4em", padding: "1em", width: "10em", cursor: "pointer"}}
        onClick={() => this.setState({ hasFocused: true }) }
      >
        <strong>
          ProClicker Fortress is ready to launch! Make the one on this button <em>Your First Click</em>!
        </strong>
        <br/>
        <br/>
        PS: Headphones recommended. Don't let your collegues find out what you're doing!
      </button>;
    }

    const marqueeJsx = <marquee>Begin your quest! Now is the time! From deep within your underground bunker, armed only with your pointer device, can you become the MASTER OF THE UNIVERSE?!?!?! Click here to find out! Make this your SECOND click!</marquee>; // eslint-disable-line

    return (
      <div style={{height: "100%", backgroundColor: "white"}}>
        <ProClickerIntroSong/>
        <div style={{paddingTop: "2em"}}>
          <img src="/logobmp.png" alt="ProClicker Fortress" width={600} />
        </div>

        <button style={{fontSize: "2em", margin: "2em 4em", padding: "1em", width: "10em"}} onClick={onClose}>
          <em>
            <strong>
              {marqueeJsx}
            </strong> 
          </em>
        </button>
        <div style={{padding: "2em", width: "50%", fontFamily: "monospace, mono"}}>
          <p>Known bugs:</p>
          <ul>
            <li>The y-ledger might be broken. That just makes it more realistic, so it's a feature.</li>
            <li>The cost popups on the clicking screen some times remain open. This seems to be a bug in blueprint js (the ui library).</li>
            <li>Overall, Pro Clicker Fortress is broken.</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default IntroPage;