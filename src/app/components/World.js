import React, { Component, Fragment } from 'react';
import actionTypes from "data/actionTypes";
import { fromJS } from "immutable";
import YouTube from 'react-youtube';

import unitGroups from "data/views/unitGroupViews";
import {
  allGovernmentActions
} from "data/views/actionTypeViews";

import TopBar from "app/components/TopBar";
import IntroPage from "app/components/IntroPage";
import UnitGroupPanel from "unit/components/UnitGroupPanel";
import YLedgerPage from "yledger/components/YLedgerPage";
import SocietyPage from "society/components/SocietyPage";
import JenkinsPanel from "app/components/JenkinsPanel";
import DebugPanel from "app/components/DebugPanel";
import InnovationPage from "innovation/components/InnovationPage";
import NewsletterPage from "newsletter/components/NewsletterPage";
import { Callout, Card, Elevation, Button } from '@blueprintjs/core';
import { checkCondition } from '../../sim/conditions';


export default class World extends Component {
  constructor(a, b) {
    super(a, b);

    this.setPage = this.setPage.bind(this);

    this.state = {
      selectedPage: "clicking"
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !this.props.world.equals(nextProps.world) || this.state || nextState;
  }

  setPage(selectedPage) {
    this.setState({selectedPage});
  }

  render() {
    const {
      world,
      intermediaries,
      setWorld,
      trigger
    } = this.props;

    const {
      selectedPage
    } = this.state;

    const production = world.get("production");
    const actions = world.get("actions");
    const units = world.get("units");
    const flags = world.get("flags");

    const disabledActions = intermediaries.getIn(["actions", "disabled"]);
    const actionProduction = production.get("actions");
    const workforceActionProduction = production.get("workforceActions");


    let pageJsx, victoryFireworks;

    const closeWinScreen = () => trigger({flags: ["win_screen_closed"]});

    if (checkCondition(world, {tech: ["win_game"], not_any_flags: ["win_screen_closed"]})) {
      victoryFireworks = (
        <div>
          <div style={{ position: "fixed", top: 0, left: 0, width: "100%", height: "100%", zIndex: 1000 }}>
            <YouTube
              videoId="XeALcNREG5g"
              opts={{
                height: '100%',
                width: '100%',
                playerVars: { // https://developers.google.com/youtube/player_parameters
                  autoplay: 1,
                  loop: 1
                }
              }}
              // onEnd={ closeWinScreen }
            />
          <div style={{ position: "fixed", top: 0, left: 0, zIndex: 10000, margin: "5em 0 0 10em" }}>
            <Button onClick={closeWinScreen}>Close Victory Screen</Button>
          </div>
            <audio autoPlay>
              <source src="/m4v-fasc.ogg" type="audio/ogg" />
            </audio>
          </div>
        </div>
      );
    }

    const showIntro = !world.getIn(["flags", "intro_closed"]);

    if (showIntro) {
      pageJsx = <IntroPage
        onClose={() => trigger({
          flags: ["intro_closed"],
          notification: {
            header: "The Ascent Begins!",
            text:
              "Can you click your way to becoming the Master of The Universe? Pro tip: ProClicker will autosave your progress by default. To manage your save games, click the ProClicker icon button in the top left corner of the screen.",
          }
        })}
      />;
    }
    else switch (selectedPage) {
      case "clicking":
        pageJsx = (
          <div>
            {
              unitGroups.map((group, id) => {

                const visibleActions = group.get("actions").filter((item, id) =>
                  intermediaries.hasIn(["actions", "visible", id]) || false);

                return (
                  <UnitGroupPanel
                    key={id}
                    groupId={id}
                    trigger={trigger}
                    unitCounts={units.get("counts")}
                    visibleActions={visibleActions}
                    disabledActions={disabledActions}
                    actionProduction={actionProduction}
                    managerAssignments={world.getIn(["actions", "managerAssignments"])}
                  >
                  </UnitGroupPanel>
                );
              }).toArray()
            }
            <JenkinsPanel bugs={units.getIn(["counts", "bug"])}/>
          </div>
        );
        break;

      case "ledger":
        pageJsx = (
          <YLedgerPage
            yledger={world.get("yledger")}
            yledgerIntermediaries={intermediaries.getIn(["yledger"]) }
            unitCounts={world.getIn(["units", "counts"])}
            appearedActions={actions.get("appeared")}
            workforceActionProduction={workforceActionProduction}
            trigger={trigger}
          />
        )
        break;

      case "society":

        pageJsx = (
          <div>
            <SocietyPage
              society={world.get("society")}
              environmentalBonuses={world.get("environmental_bonuses")}
              modifiers={intermediaries.get("modifiers")}
              trigger={trigger}
              units={units}
              visibleActions={
                allGovernmentActions.filter((item, id) =>
                  intermediaries.hasIn(["actions", "visible", id]))
              }
              disabledActions={disabledActions}
              actionProduction={actionProduction}
            />
          </div>
        )
        break;

      case "innovation":
        pageJsx = <InnovationPage
          intermediaries={intermediaries}
          flags={flags}
          trigger={trigger}
          units={units}
          disabledActions={disabledActions}
          actionProduction={actionProduction}
        />
        break;

      case "newsletters":
        pageJsx = (
          <NewsletterPage
            appearedNewsletters={world.get("appearedNewsletters")}
            notificationLog={world.get("notificationLog")}
          />
        );
        break;

      case "debug":
        pageJsx = <Fragment>
          <DebugPanel
            title="World"
            backgroundColor="#efe"
            data={world}
          />
          <DebugPanel
            title="Intermediaries"
            backgroundColor="#fef"
            data={intermediaries}
          />
          <DebugPanel
            title="Action types"
            backgroundColor="#fef"
            data={actionTypes}
          />
        </Fragment>;
        break;

      default:
        throw new Error("Unexpected page state: " + selectedPage);
    }

    return (
      <div>
        <TopBar simple={showIntro} world={world} setWorld={setWorld} page={selectedPage} setPage={this.setPage}/>
        <div style={{position: "fixed", top: "4rem", right: "1em", maxWidth: "30em", zIndex: "10000"}}>
          {
            (world.getIn(["notifications"]) || fromJS([]))
              .map((notification, i) =>
                <Card key={i} style={{ padding: "0", verticalAlign: "top", margin: "1rem" }} elevation={Elevation.TWO}>
                  <Callout intent={notification.get("intent") || "primary"} title={notification.get("header")}>
                    {notification.get("text")}
                    {
                      notification.get("sample")
                      && <audio autoPlay ref={this.audioRef}>
                        <source src={notification.get("sample")} type="audio/mpeg" />
                      </audio>
                    }
                  </Callout>
                </Card>
            ).toJS()
              }
        </div>
        { pageJsx }
        { victoryFireworks }
      </div>
        );
      }
    }
