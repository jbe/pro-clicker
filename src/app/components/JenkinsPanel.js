import React, {Component} from 'react';
import Panel from "ui/components/Panel";

export default class JenkinsPanel extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  render() {
    const {
      bugs
    } = this.props;

    const imgSrc = bugs
                 ? "/img/master-jenkins-normal3.svg"
                 : "/img/jenkins.png";

    return (
        <Panel className="JenkinsPanel" title="Jenkins status">
          <img src={imgSrc} alt="Jenkins" width="100" height="100"/>
        </Panel>
    );
  }
}
