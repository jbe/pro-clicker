import React, { Component } from 'react';
import Panel from "ui/components/Panel";

export default class DebugPanel extends Component {

  render() {
    const {
      data,
      backgroundColor,
      title
    } = this.props;

    return (
      <Panel title={title} icon="wrench">
        <pre style={{backgroundColor, maxWidth: "21rem", overflowX: "auto", wordBreak: "normal", wordWrap: "normal"}}>
          <code style={{whiteSpace: "pre"}}>
            {JSON.stringify(data.toJS(), 2, 2)}
          </code>
        </pre>
      </Panel>
    );
  }
}
