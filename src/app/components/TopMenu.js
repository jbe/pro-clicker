import React, { Component } from 'react';
import { fromJS } from "immutable";
import ReactFileReader from 'react-file-reader';
import {
  Button,
  Popover,
  Menu,
  MenuDivider,
  MenuItem,
  Position
} from "@blueprintjs/core";
import initialWorld from "data/initialWorld";
import {
  deserialize
} from "utils";
import {
  autoSaveSlotKey,
  initialAutoSaveSlot,
  autoSaveFrequency,
  saveSlotIds
} from "data/constants";
import {
  rememberedOrInitialAutoSaveSlot,
  readSave,
  readSaveName,
  writeSave,
  downloadSave
} from "persistence";


class SaveSlotsSubMenu extends Component {
  render() {
    const {
      label,
      slotNames,
      tickedSlot,
      onSelectSlot,
      icon,
      noSlotItemLabel
    } = this.props;

    const noSlotItemJsx = noSlotItemLabel && (
      <MenuItem
        key="no-slot"
        onClick={() => onSelectSlot(null) }
        icon={tickedSlot === null ? "tick" : "blank"}
        text={noSlotItemLabel}
      />
    );

    return (
      <MenuItem icon={icon || "blank"} text={label}>
        { noSlotItemJsx }
        {
          saveSlotIds.map(id => (
            <MenuItem
              key={id}
              onClick={() => onSelectSlot(id) }
              icon={id === tickedSlot ? "tick" : "blank"}
              text={"" + id + ": " + (slotNames.get(id) || "(empty)")}
            />
          ))
        }
      </MenuItem>
    );
  }
}

export default class TopMenu extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      autoSaveSlot: rememberedOrInitialAutoSaveSlot,
      /* showUploadModal: false,*/
      slotNames: saveSlotIds.map(readSaveName)
    };

    window.patchSave = this.patchSave.bind(this);

    this.handleSave = this.handleSave.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.handleSelectAutoSave = this.handleSelectAutoSave.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.handleDownload = this.handleDownload.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }

  componentWillMount() {
    this.handleLoad(initialAutoSaveSlot || rememberedOrInitialAutoSaveSlot, true);
    this.timerHandle = setInterval(() => this.autoSave(), autoSaveFrequency);
  }

  componentWillUnmount() {
    clearInterval(this.timerHandle);
  }

  autoSave() {
    const {
      autoSaveSlot
    } = this.state;

    if (!autoSaveSlot) return;

    this.handleSave(autoSaveSlot, "Autosave: " + (new Date().toString()));
  }

  patchSave() {
    const {
      autoSaveSlot,
      slotNames
    } = this.state;

    writeSave(initialWorld.mergeDeep(readSave(autoSaveSlot || 0)), autoSaveSlot, slotNames.get(autoSaveSlot || 0));
  }

  handleSave(slot, optionalSlotName) {
    const {
      world
    } = this.props;

    const {
      slotNames
    } = this.state;

    const slotName = optionalSlotName || prompt("The contents of this slot will be overwritten! Enter a name for your saved game:",  slotNames.get(slot) || (new Date()).toString());
    if (!slotName) return;

    writeSave(world, slot, slotName);
    this.setState({slotNames: slotNames.set(slot, slotName)});
  }

  handleLoad(slot, skipConfirm) {
    const {
      setWorld
    } = this.props;

    if (!skipConfirm) {
      if (!window.confirm("Any current progress will be overwritten by the loaded game!")) return;
    }

    const world = readSave(slot);

    if (!world) !skipConfirm && alert("Hey! Nothing is saved in this slot! Go eat some waffles!");
    else setWorld(world);
  }

  handleSelectAutoSave(slot) {
    if (slot) {
      if (!window.confirm("If you switch slots, the selected slot will be immediately overwritten by your current game. Proceed?")) return;
    }

    this.setState({autoSaveSlot: slot})
    localStorage.setItem(autoSaveSlotKey, JSON.stringify(slot));
  }

  handleClear() {
    const {
      setWorld
    } = this.props;

    const {
      autoSaveSlot,
    } = this.state;

    if (autoSaveSlot) {
      if (!window.confirm("You have enabled autosave. If you restart the game, the slot will be overwritten.")) return;
    }
    else {
      if (!window.confirm("If you restart the game, any current progress is lost.")) return;
    }

    setWorld(initialWorld)
  }

  handleDownload() {
    const {
      world
    } = this.props;

    downloadSave(world, "ProClicker_savegame.pro");
  }

  handleUpload(files) {
    const {
      setWorld
    } = this.props;

    const reader = new FileReader();
    reader.onload = (e) => setWorld(fromJS(deserialize(reader.result)));
    reader.readAsText(files.item(0));
  }

  render() {

    const {
      autoSaveSlot,
      slotNames
    } = this.state;

    const menuJsx = (
      <Menu>

        <SaveSlotsSubMenu
          label="Save"
          icon="floppy-disk"
          slotNames={slotNames}
          onSelectSlot={this.handleSave}
        />

        <SaveSlotsSubMenu
          label="Load"
          slotNames={slotNames}
          onSelectSlot={this.handleLoad}
        />

        <SaveSlotsSubMenu
          label={"Auto-save " + (autoSaveSlot === null ? "disabled" : "enabled")}
          icon={autoSaveSlot ? "tick" : "blank"}
          noSlotItemLabel={"Disable auto-save"}
          slotNames={slotNames}
          tickedSlot={autoSaveSlot}
          onSelectSlot={this.handleSelectAutoSave}
        />

        <MenuItem icon="step-backward" text="Restart game" onClick={this.handleClear}/>

        <MenuDivider/>
        <MenuItem icon="cloud-download" text="Save game to file" onClick={this.handleDownload}/>
        <div onClick={e => e.stopPropagation()}>
          <MenuItem icon="cloud-upload" text="Upload game file">
            <div style={{padding: "3em"}}>
              <p style={{width: "15em", marginBottom: "3em"}}>
                {
                  autoSaveSlot == null
                  ? "Uploading a saved game will replace your current progress."
                  : "You have enabled autosave. If you upload a game, your currently selected slot will be overwritten."
                }
                <br/>
                <br/>
                There's a fun bug that prevents the upload from working if the mouse pointer is not inside this window when the file is selected.
              </p>
              <ReactFileReader fileTypes="*/*" handleFiles={(files) => this.handleUpload(files)}>
                <Button text="Select file.."/>
              </ReactFileReader>
            </div>
          </MenuItem>
        </div>
      </Menu>
    );

    return (
      <Popover content={menuJsx} position={Position.RIGHT_BOTTOM}>
        <Button style={{marginRight: "0.5em"}}>
          <img alt="ProClicker Fortress" src="/favicon.png" style={{height: "20px", width: "20px"}}/>
        </Button>
      </Popover>
    );
  }
}
