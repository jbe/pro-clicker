import React, { Component, Fragment } from 'react';
import { Tooltip, Callout, Navbar, NavbarGroup, NavbarDivider, Alignment, Position, Button } from "@blueprintjs/core";
import unitTypes from "data/unitTypes";
import { checkCondition } from "sim/conditions";
import TopMenu from "app/components/TopMenu";
import UnitCountIcons from "unit/components/UnitCountIcons";

export default class TopBar extends Component {
  render() {
    const {
      simple,
      world,
      page,
      setPage,
      setWorld
    } = this.props;

    const pageButton = (id, label, icon) =>
      <Tooltip
        position={Position.BOTTOM}
        content={label}
        target={
          <Button
            style={{marginLeft: "0.5em"}}
                  active={page === id}
            onClick={() => setPage(id)}
                  className="pt-minimal"
                  icon={icon}
          />
        }
      />

      const {
        // marketingEffect,
        reputation,
        customerAcquisitionRate,
        complaintRate,
        opportunityRate,
        customers,
        /* opportunities,*/
          /* drillRate, */
        /* rabbitHoleDepth */
      } = world.get("society").toJS();

      const newsletterEnabled = checkCondition(world, {tech: ["newsletter"]});
      const yledgerEnabled = checkCondition(world, {tech: ["the_yledger"]});


      const visibleUnits = world.getIn(["units", "appeared"]);

      return (
      <div>
        <Navbar>
          <NavbarGroup align={Alignment.LEFT}>
            <TopMenu world={world} setWorld={setWorld} />
            {/* <NavbarHeading><strong>ProClicker Fortress</strong>&trade;</NavbarHeading> */}
            {
              !simple && <Fragment>
                <NavbarDivider/>
                { pageButton("clicking",   "Clicking", "build") }
                { 
                  yledgerEnabled && pageButton("ledger", "Ledger", "timeline-bar-chart")
                }
                { pageButton("society",    "Society",  "take-action") }
                { pageButton("innovation", "Innovation",  "cog") }
                {
                  newsletterEnabled && pageButton("newsletters", "Newsletters", "notifications")
                }
                {
                  process.env.NODE_ENV === "development" && <Fragment>
                    <NavbarDivider/>
                    { pageButton("debug", "Debug",  "wrench") }
                  </Fragment>
                }
                <UnitCountIcons
                  showHelp
                  precision={0}
                  alwaysShowUnits={visibleUnits}
                  counts={unitTypes.map((unitType, id) => world.getIn(["units", "counts", id]))}
                />
              </Fragment>
            }
          </NavbarGroup>
        </Navbar>
        {
          !simple && <Callout>
            <strong style={{ borderBottom: "1px solid black" }}>
                {customers.toFixed(0)} customers
            </strong>
          {" "} &middot; {reputation.toFixed(1)} reputation
          {/* &middot; {problems} problems */}
          {/* &middot; {marketingEffect.toFixed(1)} marketing power */}
          &middot; {complaintRate.toFixed(3)} complaints/s
          &middot; {opportunityRate.toFixed(3)} opportunities/s
          &middot; {customerAcquisitionRate.toFixed(3)} customers/s
        </Callout>
        }
      </div>
    );
  }
}
