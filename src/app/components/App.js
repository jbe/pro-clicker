import React, { Component } from 'react';
import {
  rememberedOrInitialAutoSaveSlot,
  readSave,
} from "persistence";

import initialWorld from "data/initialWorld";
import { tickLength } from "data/constants";
import { applyEffect } from "sim/effects";
import worldTick from "sim/worldTick";
import intermediariesTick from "sim/intermediariesTick";

import World from "app/components/World";

class App extends Component {
  constructor(a, b) {
    super(a, b);

    this.tick  = this.tick.bind(this);
    this.trigger = this.trigger.bind(this);

    const world = readSave(rememberedOrInitialAutoSaveSlot) || initialWorld;
    const intermediaries = intermediariesTick(world);

    this.state = {
      world,
      intermediaries
    };

    window.trigger = this.trigger.bind(this);
  }

  componentDidMount() {
    this.timerHandle = setInterval(this.tick, tickLength * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerHandle);
  }

  trigger(effect) {
    const { world } = this.state;
    const newWorld = applyEffect(world, effect);
    this.setState({world: newWorld});
  }

  tick() {
    const {
      world: oldWorld,
      intermediaries: oldIntermediaries
    } = this.state;
    let world, intermediaries;

    // time runs twice as fast in post quantum societies
    if (oldWorld.getIn(["society", "government"]) === "post_quantum") {
      world = worldTick(oldWorld, oldIntermediaries);
      intermediaries = intermediariesTick(world);
    }

    world = worldTick(oldWorld, oldIntermediaries);
    intermediaries = intermediariesTick(world);

    this.setState({
      world,
      intermediaries
    });
  }

  render() {
    const {
      world,
      intermediaries
    } = this.state;

    return (
      <div className="App">
        <World
          world={world}
          intermediaries={intermediaries}
          trigger={this.trigger}
          setWorld={(world) => this.setState({world, intermediaries: intermediariesTick(world)}) }
        />
      </div>
    );
  }
}

export default App;
