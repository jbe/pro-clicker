import React, {Component, Fragment} from 'react';
import Panel from "ui/components/Panel";
import ActionTriggers from "action/components/ActionTriggers";
import governmentTypes from "data/governmentTypes";
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";


class GovernmentSelector extends Component {
  render() {
    const {
      // current,
      units,
      visibleActions,
      disabledActions,
      trigger,
      actionProduction
    } = this.props;

    if (!visibleActions.size) return (
      <p>
        Research more government types to enable societal changes.
      </p>
    );

    return (
      <div>
        <hr/>
          <p>
            We are ready to create the changes needed to transition into the following government types:
          </p>
        <hr/>
        <ActionTriggers
          units={units}
          visibleActions={visibleActions}
          disabledActions={disabledActions}
          trigger={trigger}
          actionProduction={actionProduction}
        />
      </div>
    );
  }
}


export default class SocietyPage extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  render() {
    const {
      environmentalBonuses,
      modifiers,
      society,
      units,
      visibleActions,
      disabledActions,
      trigger,
      actionProduction
    } = this.props;

    const currentGovernment = governmentTypes.get(society.get("government"));

    const filteredActions = visibleActions.filter((v, k) => k !== "government_" + society.get("government"));

    return (
      <Fragment>
        <Panel title="Society" icon="take-action" maxWidth="30em">
          <table className="pt-html-table">
            <tbody>
              <tr>
                <th>Current government</th>
                <td>
                  <strong>
                    {currentGovernment.get("header")}
                  </strong>
                  <br/>
                  <small>{currentGovernment.get("help")}</small>
                </td>
              </tr>
              <tr>
                <th>Bonuses</th>
                <td>
                  {
                    environmentalBonuses.get("golden_age") &&
                      <p>
                        <strong>Golden Age</strong>
                        <br />
                        <small>
                          The company is in a golden age of prosperity. Research speed is doubled.
                        </small>
                      </p>
                  }
                  {
                    (modifiers.get("complaintMultiplier") !== 1) &&
                      <p>Complaints &times; {modifiers.get("complaintMultiplier")}</p>
                  }
                  {
                    (modifiers.get("wagesMultiplier") !== 1) &&
                      <p>Wages &times; {modifiers.get("wagesMultiplier")}</p>
                  }
                  {
                    modifiers.get("unitWagesMultiplier").map((mult, unit_id) => {
                      return (mult !== 1) && <p>{unitTypes.getIn([unit_id, "plural"])} upkeep &times; {mult}</p>;
                    }).valueSeq().toJS()
                  }
                  {
                    modifiers.get("actionProductionMultipliers").map((mult, action_id) => {
                      return (mult !== 1) && <p>{actionTypes.getIn([action_id, "action_text"])} efficiency &times; {mult}</p>;
                    }).valueSeq().toJS()
                  }
                </td>
              </tr>
            </tbody>
          </table>
        </Panel>

        {
          (visibleActions && visibleActions.size) &&
            <Panel title="Revolution" icon="flag" maxWidth="26em">
              <GovernmentSelector
                current={society.get("government")}
                units={units}
                visibleActions={filteredActions}
                disabledActions={disabledActions}
                trigger={trigger}
                actionProduction={actionProduction}
              />
            </Panel>
        }
      </Fragment>
    );
  }
}
