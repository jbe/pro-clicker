import React from 'react';
import {fromJS} from "immutable";

export default fromJS({


  // Generic

  newsletter_established: {
    header: "Company Newsletter Established",
    image: "newsletter.jpg",
    trigger: {
      tech: ["newsletter"]
    },
    text: <div>
      <p>
        Hello everyone! We are the company newsletter editors. Just thought we would send you a newsletter and say hello. We will be providing you with fresh news stories from now on. Stay frosty out there and remember to take vitamins!
      </p>
    </div>
  },

  jenkins_unhappy: {
    header: "Master Jenkins is unhappy",
    image: "master-jenkins-normal3.svg",
    trigger: {
      minUnits: {
        bug: 50
      }
    },
    text: <div>
      <p>During a public address last friday, Master Jenkins professed his deep sadness over recent company developments. "All in all, I am not very happy at all", the butler told reporters. Jenkins has always been a watchful presence over our company, and we are sorry to see him like this. We all wish him a speedy recovery.</p>
    </div>
  },

  business_is_great: {
    header: "Business is great!",
    image: "business-is-great.jpg",
    trigger: {
      minUnits: {kroon: 1500}
    },
    text: <div>
      <p>Business is business, but these days business is great! We are doing great! So many new customers, so much kroons rolling in. All in all, we're doing pretty well.</p>
    </div>
  },

  the_sky_is_the_limit: {
    header: "The sky is the limit",
    image: "sky-is-limit.jpg",
    trigger: {
      minUnits: {kroon: 10000}
    },
    text: <div>
      <p>Business has been good for some time, but now it is even better. This proves that the sky is the limit when it comes to our business. Stay sharp out there, and remember to TCB!</p>
    </div>
  },

  new_markets_conquered: {
    header: "New markets conquered!",
    image: "success.jpg",
    trigger: {
      minUnits: {kroon: 50000}
    },
    text: <div>
      <p>Business is great as usual these days, and we are expanding into more and more markets. Customers are practically overflowing into our company.</p>
    </div>
  },

  a_golden_age_of_business: {
    header: "A golden age of business",
    image: "mind-power.jpg",
    trigger: {
      minUnits: {kroon: 150000}
    },
    text: <div>
      <p>A golden age of business has descended upon our company, and all workers rejoice in a prosperous wave of innovation and inspiration. Researchers everywhere are burning with passion for knowledge, and our workers are motivated and strong. Everyone looks brightly towards the future.</p>
    </div>
  },

  research_lab_established: {
    header: "Possier Dofile establishes research lab",
    image: "lab-1.jpg",
    trigger: {all_flags: ["research_enabled"]},
    text: <div>
      <p>Numerous vats, bulbs, hoses and metal implements caked in dust cover the tables and cupboards around us, as I sit down to speak with Roy the Researcher, of the newly completed Possier Dofile Research Lab. "Ahh this place is A DREAM!" he declares. "Come here, look at these structures that I have formed out of mere liquids!" He gestures towards some complex topology extending out of one of the larger beakers behind him. "And look at this!" A big metal cage rattles in the corner. "A real live crocodile, imagine the possibilities!"</p>
    </div>
  },

  back_ends_show_promise: {
    header: "Back ends show promise",
    image: "back-end.jpg",
    trigger: {minUnits: {back_end: 1}},
    text: <div>
      <p>I have visited the lab to take part in its experiments before, but what I see before me this time defies all logic. An immense contraption, riddled with gearwheels, shafts, levers and cranks, crunching away at hidden problems by its own byzantine logic, lost in an internal world of unimaginable complexity. The towering backend rattles and hums before me like a dreaming behemoth.</p>
      <p>It is the latest and certainly not the least invention to come out of our research lab. "I don't even know what this wheel does," Roy the Researcher explains, pointing to one of the larger modules of moving parts. "But it seems to follow the fibonacci sequence. It will surely take many years to fully understand and apply this machine, and perhaps we never really will."</p>
      <p>One thing is certain; the back end is here to stay.</p>
    </div>
  },

  distgruntled_workers_pungent_documents: {
    header: "Workers disgruntled by pungent documents",
    image: "fermented.jpg",
    trigger: {minUnits: {fermented_sp: 4, alcohol: 1}},
    text: <div>
      <p>Several angry workers rallied outside the supreme office this morning, to protest against their working conditions, which they described as "unbearable" and "asphyxiating". The protesters were apprehended and shipped off for reconditioning. We would like to remind all workers to stay productive.</p>
    </div>
  },

  binge_drinking: {
    header: "Tip of the day",
    image: "binge-drinking.jpg",
    trigger: {minUnits: {alcohol: 10}},
    text: <div>
      <p>Did you know that binge drinking can improve your productivity by as much as 81 percent? Alcohol is now abundant at the company, thanks to prolonged fermentation efforts.</p>
    </div>
  },

  workers_concerned_for_own_safety: {
    header: "Workers concerned for safety",
    image: "crocodile.jpg",
    trigger: {minUnits: {crocodile: 1}},
    text: <div>
      <p>Another angry mob gathered outside the supreme office this morning, protesting against dangerous working conditions. Several frontend developers have lost limbs over the last few weeks, and a few have died. The protesters have been shipped to reconditioning.</p>
    </div>
  },

  workers_complain_about_conditions: {
    header: "Workers complain about conditions",
    image: "birdshit.jpg",
    trigger: {minUnits: {kolibri: 20}},
    text: <div>
      <p>Another wave of protesters were observed earlier this morning. They all seemed to be covered in bird excrement, and were not taken seriously by anyone. No response was necessary to restore order.</p>
    </div>
  },

  rare_kolibri_spotted: {
    header: "Rare kolibri spotted",
    image: "kolibri.jpg",
    trigger: {minUnits: {kolibri: 40}},
    text: <div>
      <p>A rare Kolibri of the Kolibrus Quirrevittus species was spotted nesting in some specifications earlier this week. Big thanks to an anonymous contributor for this beautiful picture!</p>
    </div>
  },

  ballmer_advisor: {
    header: "Steve Ballmer to become first offical advisor",
    image: "ballmer-rage.jpg",
    trigger: {tech: ["ballmer"]},
    text: <div>
      <p>Rumours have circulated for some time after ex-Microsoft CEO Steve Balmer was visiting the Supreme Leader and Clicking Presence last week. We, the Company Newsletter, are indeed the first to break the news of a partnership today. Ballmer has agreed to act as an official company advisor, for a significant amount of kroons. "This paves the way for expansion into new segments, and opens up new markets, such as firearms," the secretary told The Newsletter. "We are certainly excited about the possibilities."</p>
    </div>
  },

  dark_web: {
    header: "Shadowy connections",
    image: "dark-web.jpg",
    trigger: {tech: ["ballmer"]},
    text: <div>
      <p>Thanks to Steve Ballmer, our company intranet is now plugged staight into the Dark Web, and the two have started to merge.</p>
    </div>
  },

  weapons_acquired: {
    header: "Guns acquired!",
    image: "weapons.jpg",
    trigger: {minUnits: {weapon: 1}},
    text: <div>
      <p>An old dream became reality yesterday, when a huge shipment of weapons arrived by cargo plane, all the way from one of our suppliers in Belarus.</p>
    </div>
  },

  age_of_biotech: {
    header: "A new age of biotech",
    image: "cloning-vat.png",
    trigger: {tech: ["biotech"]},
    text: <div>
      <p>The low hum from the vats is soothing.</p>
    </div>
  },

  heino_cloning_ethics: {
    header: "Competitors raise ethical concerns over Heino Cloning",
    image: "heino.jpg",
    trigger: {minUnits: {heino: 1}},
    text: <div>
      <p>One of our competitors, "Doorfus Poofile for Business&trade;" has been spreading revisionist propaganda, claiming that our ethical Heino Cloning is in fact unethical. Among the concerns raised are 1) the well-being of the Heino's themselves, 2) certain modification to the Heino's genetic material, and 3) the potential effects of a large number of Heinos within a human population.</p>
    </div>
  },

  basement_stench: {
    header: "Workers disgruntled over \"unbearable\" basement stench, golem rumours",
    image: "golem.jpg",
    trigger: {minUnits: {golem: 1}},
    text: <div>
      <p>Several workers rallied for a demonstration outside the Supreme Office yesterday, to protest against what they called "An unbearable stench from the basement". All workers were shipped to reconditioning, and are happily reintegrated into their social units.</p>
    </div>
  },

  dump_golem_rampage: {
    header: "Dump golem rampage!",
    image: "rampage.jpg",
    trigger: {minUnits: {golem: 3}},
    text: <div>
      <p>You would have had to be there to truly get a grasp of the level of noise and commotion at yesterday's military parade. "People started to panic as the number of golems kept increasing, but the ground was too slippery for anyone to move, and the golems kept flowing in. It was a true nightmare before our eyes," The Newsletter learned from an anonymous bystander.</p>
      <p>Our competitors will fear us even more now.</p>
    </div>
  },

  that_which_should_not_exist: {
    header: "That which should not exist",
    image: "abomination.jpg",
    trigger: {minUnits: {abomination: 2}},
    text: <div>
      <p>There is something in the air. Something seems to stick to the walls. My breath has grown heavier and I don't know why. I keep reacting to sounds, which turn quiet as soon as I try to recognize them. Something is unfinished. Something has failed to manifest. It hovers like a ghost; like an <em>unpersisted abomination</em>.</p>
    </div>
  },

  super_soldiers: {
    header: "Say hello to the Super Soldiers",
    image: "pro-soldiers.jpg",
    trigger: {minUnits: {pro_soldier: 1}},
    text: <div>
      <p>
        Vacant stares from glassy eyes. These beings are neither human nor animal.
      </p>
      <p>
        They are pro-soldiers; the latest addition to our company militia, and one of the most fearsome foot soldiers in the known universe. Based on genetically modified Heino clones, imbued with alcohol and supplied with weapons and hard training from birth, they are singularly bound by the will of their master.
      </p>
    </div>
  },

  weaponized_dragon: {
    header: "Dragon successfully weaponized",
    image: "pro-dragon.jpg",
    trigger: {minUnits: {pro_dragon: 1}},
    text: <div>
      <p>Take one un-weaponized dragon, fresh from the InputFormElementNest, and implant it with a multitude of weapons, artificial organs, aggression enhancers, brain stimulators/controllers, and and a solid 4g uplink. What are you left with? A pro-dragon of course.</p>
    </div>
  },

  nerds_calmed_by_implants: {
    header: "Raging nerds finally calmed by implants",
    image: "raging-nerd.jpg",
    trigger: {tech: ["motivator_implant"]},
    text: <div>
      <p>A problem presumably as old as the universe itself was conclusively solved by our pioneering scientists yesterday. Raging nerds are now a thing of the past, and a curious calmness permeates every office space in our empire.</p>
    </div>
  },

  leadership_bent_on_domination: {
    header: "Leadership bent on galactic domination",
    image: "galaxy.jpg",
    trigger: {minUnits: {pro_tac_team: 1}},
    text: <div>
      <p>"One world is not enough," company leaders and investors were heard chanting at a recent quarterly meeting.</p>
    </div>
  },

  wormhole_opens: {
    header: "Wormhole opens",
    image: "wormhole.webp",
    trigger: {tech: ["warp_gate"]},
    text: <div>
      <p>
        Roy the researcher sits in his chair, twisting his long white beard with two fingers. "Look what I made", he says. "It wasn't even that hard. I just folded a supermonadic particle onto itself an infinite number of times with this very geometrically interesting magnetic field to produce antigravity, and the rest was completely trivial. It did require a lot of power though, thanks for that!"
      </p>
      <p>
        The Supreme Clicker and Boss has been thirsting for galactic domination for a long time, and today, as the wormhole opens, our expansion takes off, to change the universe for ever.
      </p>
    </div>
  },

  supercluster_within_grasp: {
    header: "Local supercluster within grasp",
    image: "supercluster.jpg",
    trigger: {tech: ["win_game"]},
    text: <div>
      <p>Since the galaxy became dominated by Possier Dofile earlier this week, the supreme Boss that Clicks has set his sights on the local supercluser. Following reports this morning, it seems like this little pocket of the known universe will soon be ours!</p>
      <p>Congratulations, you win the game! That's right, you just won a clicker game!</p>
    </div>
  },

});