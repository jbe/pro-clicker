import {fromJS} from "immutable";
import technologies from "data/technologies";

export default fromJS({

  code_trap: {
    help: "You build a trap."
  },

  research_enabled: {
    help: "Enables research."
  },

  fight_king: {
    help: "What might happen is anyone's guess."
  },

  flee_king: {
    help: "Shame."
  }

}).merge(technologies
  .mapKeys((k) => "researched_" + k)
  .map((item, id) => fromJS({
    help: item.get("flag_help"),
  }))
);

// TODO: can merge with opportunity flags as well
