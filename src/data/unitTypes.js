import React from 'react';
import { fromJSOrdered } from "utils";

export default fromJSOrdered({

  /* Units */

  kroon: {
    category: "resource",
    singular: "Kroon",
    plural: "Kroons",
    help: "The primary monetary denomination of the Serfdom of Norbay.",
    icon: "ring",
    initial: 0,
    appear_when: {minUnits: {kroon: 1}}
  },

  alcohol: {
    category: "resource",
    singular: "Alcohol unit",
    plural: "Alcohol units",
    help: "Produced by distilling fermented SP's.",
    icon: "tint",
    initial: 0,
    appear_when: {tech: ["distillation"]}
  },

  loc: {
    category: "resource",
    singular: "Line of Code",
    plural: "Lines of Code",
    help: "The wellspring of everything in life.",
    icon: "code",
    initial: 0,
  },

  sp: {
    category: "resource",
    singular: "SP",
    plural: "SP's",
    help: "Short for Solution Proposal. It's a document.",
    icon: "applications",
    initial: 0,
    appear_when: {tech: ["sp_writing"]}
  },

  feature: {
    category: "resource",
    singular: "Feature",
    plural: "Features",
    help: "Attracts customers. Produced from lines of code and can be sold.",
    icon: "box",
    initial: 0,
    appear_when: {tech: ["features"]}
  },

  back_end: {
    category: "resource",
    singular: "Back end",
    plural: "Back ends",
    help: "Produces ethereal resources and can be used to build other units. Polished military-grade steel.",
    icon: "one-column",
    initial: 0,
    actionContributions: { 
      manual_dump: 0.01,
      day_dream: 0.005,
      deja_vu: 0.004
    },
    appear_when: {tech: ["the_back_end"]}
  },

  bug: {
    category: "resource",
    singular: "Bug",
    plural: "Bugs",
    help: "Registered by support guys in response to complaints.",
    icon: "error",
    initial: 0,
    appear_when: {minUnits: {bug: 1}}
  },

  opportunity: {
    category: "resource",
    singular: "Opportunity",
    plural: "Opportunities",
    help: "Customers generate sales opportunities. Opportunities can be used to sell features.",
    icon: "credit-card",
    initial: 0,
    appear_when: {tech: ["features"]}
  },

  complaint: {
    category: "resource",
    singular: "Complaint",
    plural: "Complaints",
    help: "Filed by customers when the solution does not fit their crummy visions. They are converted to bugs by support guys.",
    icon: "hand-up",
    initial: 0,
    appear_when: {minUnits: {complaint: 1}}
  },


  /* Exotic units */

  brain: {
    category: "exotic",
    singular: "Brain",
    plural: "Brains",
    help: "I wonder what these are for.",
    icon: "predictive-analysis",
    initial: 1,

    appear_when: {minUnits: {brain: 1}},
  },

  fermented_sp: {
    category: "exotic",
    singular: "Fermented SP",
    plural: "Fermented SP's",
    help: "Pungent documents having spent too much time in their own company.",
    icon: "heatmap",
    initial: 0,

    appear_when: {tech: ["fermentation"]}
  },

  tapestry: {
    category: "exotic",
    singular: "Tapestry",
    plural: "Tapestries",
    help: "A new front end architecture in the form of beautiful ornate textiles.",
    icon: "grid",
    initial: 0,

    appear_when: {tech: ["tapestries"]}
  },

  crocodile: {
    category: "exotic",
    singular: "Crocodile",
    plural: "Crocodiles",
    help: "A new front end architecture based on dangerous acquatic reptiles.",
    icon: "drag-handle-horizontal",
    initial: 0,

    appear_when: {tech: ["crocodiles"]}
  },

  kolibri: {
    category: "exotic",
    singular: "Kolibri",
    plural: "Kolibris",
    help: "Another front end architecture based on sweet little birds.",
    icon: "alignment-horizontal-center",
    initial: 0,

    appear_when: {tech: ["kolibris"]}
  },

  ifen: {
    category: "exotic",
    singular: "InputFormElementNest",
    plural: "InputFormElementNests",
    help: "A generic form input element in the Kolibri framework that attracts dragons.",
    icon: "widget-button",
    initial: 0,

    appear_when: {tech: ["input_form_element_nests"]}
  },

  dragon: {
    category: "exotic",
    singular: "Dragon",
    plural: "Dragons",
    help: "Dragons are born in InputFormElementNests. They are quite harmless unless weaponized.",
    icon: "eye-on",
    initial: 0,

    appear_when: {tech: ["input_form_element_nests"]}
  },

  weapon: {
    category: "exotic",
    singular: "Weapon",
    plural: "Weapons",
    help: "Used by military units.",
    icon: "locate",
    initial: 0,

    appear_when: {tech: ["ballmer"]}
  },

  heino: {
    category: "exotic",
    singular: "Heino",
    plural: "Heinos",
    help: "Germanic schlager artist and a primeval life force. Heino's attract customers!",
    icon: "walk",
    initial: 0,

    /* appear_when: state => state.tech.schlager,*/

    appear_when: {tech: ["schlager"]}
  },


  /* Means of production */


  developer: {
    category: "workforce",
    singular: "Developer",
    plural: "Developers",
    help: <span>
      Writes code and SP's; and more! They also pick bugs off each other's code bases.<br/> Given enough time, they can produce any program imaginable; even the collected works of Shakespeare.
    </span>,
    icon: "mugshot",
    upkeep: {kroon: 0.2},
    initial: 0,

    actionContributions: {
      develop: 1,
      write: 1,
      ship: 1,
      assemble: 1,
      weave: 1,
      breed_crocodile: 1,
      hatch_kolibri: 1,
      create_ifen: 1,
      squish: 1,
    },
    contributionAllocationStatePath: ["yledger", "allocations"],

    appear_when: {tech: ["the_yledger"]},
  },

  support: {
    category: "workforce",
    singular: "Support person",
    plural: "Support guys",
    help: "Take care of complaints, by converting them to bugs.",
    icon: "issue-closed",
    upkeep: {kroon: 0.2},
    initial: 0,

    actionContributions: {"analyze": 1},

    appear_when: {tech: ["subordinates"]}
  },

  salesperson: {
    category: "workforce",
    singular: "Salesperson",
    plural: "Salespeople",
    help: "A special kind of person that has evolved to be able to sell features.",
    icon: "headset",
    upkeep: {kroon: 0.5},
    initial: 0,

    actionContributions: {sell: 1},

    appear_when: {tech: ["subordinates"]}
  },

  marketer: {
    category: "workforce",
    singular: "Marketer",
    plural: "Marketers",
    help: "Cunning masters of manipulation, marketers work singlemindedly towards slowly affecting changes in the brains of the general public.",
    icon: "media",
    upkeep: {kroon: 0.7},
    initial: 0,

    appear_when: {tech: ["subordinates", "NEVER - disabled for now"]}
  },

  manager: {
    category: "workforce",
    singular: "Manager",
    plural: "Managers",
    help: "They click buttons so you don't have to!",
    icon: "briefcase",
    upkeep: {kroon: 1},
    initial: 0,

    appear_when: {tech: ["brain_extraction"]}
  },

  // raging_nerd: {
  //   category: "workforce",
  //   singular: "Raging nerd",
  //   plural: "Raging nerds",
  //   help: "Transfigured developers who lost their minds after being underpaid, or in some deep rabbit hole of code.",
  //   icon: "blocked-person",
  //   initial: 0,
  //   appear_when: {minUnits: {raging_nerd: 1}}
  // },

  /* Ethereal objects */

  dump: {
    category: "ethereal",
    singular: "Dump",
    plural: "Dumps",
    help: "Produced semi-randomly by back ends.",
    icon: "cloud",
    initial: 0,

    appear_when: {minUnits: {dump: 1}}
  },

  unpersisted_achievement: {
    category: "ethereal",
    singular: "Unpersisted achievement",
    plural: "Unpersisted achievements",
    help: "Achievements that were never real.",
    icon: "heart-broken",
    initial: 0,
    appear_when: {minUnits: {unpersisted_achievement: 1}}
  },

  unpersisted_checklist_item: {
    category: "ethereal",
    singular: "Unpersisted checklist item",
    plural: "Unpersisted checklist items",
    help: "These were ticked off in another dimension. Now they haunt us here.",
    icon: "random",
    initial: 0,
    appear_when: {minUnits: {unpersisted_checklist_item: 1}}
  },

  database_connection: {
    category: "ethereal",
    singular: "Database connection",
    plural: "Database connections",
    help: "Produced randomly by backends.",
    icon: "database",
    initial: 0,
    appear_when: {minUnits: {database_connection: 1}}
  },


  /* Military */

  bomber: {
    category: "military",
    singular: "Magic Carpet Bomber",
    plural: "Magic Carpet Bombers",
    help: "The standard heavy bomber of the ProArmada.",
    icon: "airplane",
    initial: 0,

    // cost: 10 tapestries, alcohol, heino, weapons, back end
    appear_when: {tech: ["summoning"]}
  },

  pro_soldier: {
    category: "military",
    singular: "ProSoldier",
    plural: "ProSoldiers",
    help: "The basic army super soldier. Created by bionically enhancing a Heino clone.",
    icon: "badge",
    initial: 0,

    appear_when: {tech: ["military", "schlager"]}
  },

  pro_dragon: {
    category: "military",
    singular: "ProDragon",
    plural: "ProDragons",
    help: "A mammoth feat of engineering, few dare to oppose the lumbering ProDragon.",
    icon: "inner-join",
    initial: 0,

    appear_when: {tech: ["military", "summoning"], minUnits: {dragon: 1}}
  },

  abomination: {
    category: "military",
    singular: "Unpersisted abomination",
    plural: "Unpersisted abominations",
    help: "A twitching mass of things which should not exist, ever hungry for life.",
    icon: "pulse",
    initial: 0,
    appear_when: {tech: ["summoning"]}
  },

  golem: {
    category: "military",
    singular: "Dump golem",
    plural: "Dump golems",
    help: "The stench is unbearable, they leave a trail where ever they go.",
    icon: "graph",
    initial: 0,
    appear_when: {tech: ["summoning"]}
  },

  archon: {
    category: "military",
    singular: "Archon",
    plural: "Archons",
    help: "A being of overwhelming power.",
    icon: "social-media",
    initial: 0,
    appear_when: {tech: ["pair_programming"]}
  },

  pro_tac_team: {
    category: "military",
    singular: "ProTacTeam",
    plural: "ProTacTeams",
    help: "A military unit working together.",
    icon: "layout-group-by",
    initial: 0,
    appear_when: {tech: ["military", "summoning"]}
  },


  colony: {
    category: "military",
    singular: "Colonized world",
    plural: "Colonized worlds",
    help: "Worlds developed to our high standards.",
    icon: "globe",
    initial: 0,

    appear_when: {tech: ["warp_gate"]}
  }

});
