// import { fromJS } from "immutable";
import { fromJSOrdered } from "utils";

function effectFlagProps(key, additionalEffects={}) {
  return {
    visible_when: {not_all_flags: [key]},
    effect: {flags: [key], ...additionalEffects}
  };
}

// function multiChoice({
//   prefix,
//   category,
//   header,
//   appear_when,
//   choices
// }) {
//   return fromJSOrdered(choices)
//   .mapKeys(k => prefix + "_" + k)
//   .map((v, k) => {
//     return fromJS({
//       category: category || "opportunity",
//       header: header + ": " + v.get("header"),
//       story_text: v.get("story_text"),
//       action_text: v.get("action_text"),
//       time: 0,
//       cost: v.get("cost"),
//       appear_when,

//       visible_when: {
//         not_any_flags: fromJS(choices).map((v) => v.get("flag"))
//       },
//       effect: fromJS(v.get("effect") || {}).set("flags", [v.get("flag")])
//     });
//   });
// }


// Opportunities

export default fromJSOrdered({
  code_trap: {
    category: "opportunity",
    header: "Just an idea",
    story_text: "Looks like you have some lines of code! Let's assemble a baited trap, and catch ourselves a worker.",
    action_text: "Build trap",
    cost: {loc: 10},
    time: 20,
    appear_when: {minUnits: { loc: 10}},

    ...effectFlagProps("code_trap")
  },

  get_roy: {
    category: "opportunity",
    header: "You caught a developer!",
    story_text: "A curious worker got stuck in your trap! He seems smart, may I recommend putting him to work in the abandoned laboratory?",
    action_text: "To the lab!",
    cost: {loc: 20},
    time: 20,
    appear_when: {all_flags: ["code_trap"]},

    ...effectFlagProps("research_enabled", {
      notification: {
        sample: "/tech_jingle.ogg",
        header: "Research enabled!",
        text: "You can now research technologies in the laboratory!",
      }
    })
  },

});
  // .merge(multiChoice({
  //   prefix: "king_of_back_ends",
  //   header: "The King of Back Ends",
  //   appear_when: {minUnits: {back_end: 25}},

  //   choices: {

  //     fight: {
  //       header: "Fight",
  //       story_text: "The King looks dangerous. It's hard to tell who would win in advance..",
  //       action_text: "En garde!",
  //       cost: {sp: 5, back_end: 25},
  //       flag: "fight_king"
  //     },

  //     flee: {
  //       header: "Flee",
  //       story_text: "If you choose to run away, you will have to live with the bugs of shame..",
  //       action_text: "Run away",
  //       flag: "flee_king",
  //       effect: {
  //         units: {
  //           bug: 50
  //         }
  //       }
  //     },
  //   }
  // }))
