
import actionTypes from "data/actionTypes";

// import { fromJS } from "immutable";
// const actionTypes = fromJS({});

function filterActionTypes(category) {
  return actionTypes.filter((item, id) => item.get("category") === category);
}

export const allOpportunityActions        = filterActionTypes("opportunity");
export const allResearchActions           = filterActionTypes("research");
export const allGovernmentActions         = filterActionTypes("government");
