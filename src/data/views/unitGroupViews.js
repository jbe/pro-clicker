import {fromJS} from "immutable";
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";

function filterUnitTypes(category) {
  return unitTypes.filter((item, id) => item.get("category") === category);
}

function filterActionTypes(category) {
  return actionTypes.filter((item, id) => item.get("category") === category);
}

function filterUnitAndActionTypes(category) {
  return {
    units: filterUnitTypes(category),
    actions: filterActionTypes(category)
  };
}


export default fromJS({
  resource: {
    header: "Resources",
    ...filterUnitAndActionTypes("resource")
  },

  mop: {
    header: "Workforce",
    ...filterUnitAndActionTypes("workforce")
  },

  exotic_resource: {
    header: "Exotic resources",
    ...filterUnitAndActionTypes("exotic")
  },

  abstract: {
    header: "Ethereal resources",
    ...filterUnitAndActionTypes("ethereal")
  },

  military: {
    header: "Military",
    ...filterUnitAndActionTypes("military")
  },

});
