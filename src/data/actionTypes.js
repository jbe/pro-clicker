import { fromJS } from "immutable";
import { fromJSOrdered } from "utils";
import technologies from "data/technologies";
import opportunities from "data/opportunities";
import governmentTypes from "data/governmentTypes";

export default fromJSOrdered({

  develop: {
    category: "resource",
    action_text: "Develop LoC's",
    time: 1,
    cost: {},
    effect: {units: {loc: 3}}
  },

  write: {
    category: "resource",
    action_text: "Write SP",
    time: 5,
    cost: {},
    effect: {units: {sp: 1}},
    appear_when: {tech: ["sp_writing"],},
  },

  distill: {
    category: "resource",
    action_text: "Distill alcohol",
    time: 45,
    cost: {fermented_sp: 1},

    effect: {units: {alcohol: 1}},
    appear_when: {tech: ["distillation"],},
  },

  squish: {
    category: "resource",
    action_text: "Squish bug",
    time: 1, // TODO
    cost: {loc: 15},
    appear_when: {tech: ["debugging"]},
    enabled_when: {minUnits: {bug: 1}},
    effect: {units: {bug: -1}}
  },

  assemble: {
    category: "resource",
    action_text: "Assemble back end",
    time: 60 * 5,
    cost: {sp: 20, loc: 750},
    effect: {units: {back_end: 1}},
    appear_when: {tech: ["the_back_end"],},
  },

  ship: {
    category: "resource",
    action_text: "Ship feature",
    time: 10,
    cost: { sp: 1, loc: 25 },
    effect: {units: {feature: 1}},
    appear_when: {tech: ["features"],},
  },

  sell: {
    category: "resource",
    action_text: "Sell feature",
    time: 15,
    cost: { feature: 1, opportunity: 1 },
    effect: {units: {kroon: 125}},
    appear_when: {tech: ["features"],},
  },

  analyze: {
    category: "resource",
    action_text: "Analyze complaint",
    time: 30,
    cost: { complaint: 1 },
    effect: {units: {bug: 1,}},
    appear_when: {minUnits: {complaint: 1},},
  },

  // Exotic

  ferment: {
    category: "exotic",
    action_text: "Ferment SP",
    time: 45,
    cost: {sp: 1},
    effect: {units: {fermented_sp: 1}},
    appear_when: {tech: ["fermentation"],},
  },

  weave: {
    category: "exotic",
    action_text: "Weave tapestry",
    time: 60,
    cost: {sp: 1, loc: 500},
    effect: {units: {tapestry: 1}},
    appear_when: {tech: ["tapestries"]},
  },

  breed_crocodile: {
    category: "exotic",
    action_text: "Breed crocodiles",
    time: 60,
    cost: {sp: 1, loc: 750},
    effect: {units: {crocodile: 2}},
    appear_when: {tech: ["crocodiles"]},
  },

  hatch_kolibri: {
    category: "exotic",
    action_text: "Hatch Kolibris",
    time: 60,
    cost: {kroon: 500, sp: 5, loc: 1000},
    effect: {units: {kolibri: 5}},
    appear_when: {tech: ["kolibris"]},
  },

  create_ifen: {
    category: "exotic",
    action_text: "Create InputFormElementNest",
    time: 60,
    cost: { kolibri: 10, loc: 1500 },
    effect: {units: {ifen: 1}},
    appear_when: {tech: ["input_form_element_nests"]},
  },

  buy_weapon: {
    category: "exotic",
    action_text: "Buy Weapon",
    time: 30,
    cost: {kroon: 500},
    effect: {units: {weapon: 1}},
    appear_when: {tech: ["ballmer"]},
  },

  clone_heino: {
    category: "exotic",
    action_text: "Clone Heino",
    time: 30,
    cost: {kroon: 500, alcohol: 5, developer: 1},
    effect: {units: {heino: 2}},
    appear_when: {tech: ["schlager"]},
  },

  // Workforce

  hire_dev: {
    category: "workforce",
    action_text: "Hire developer",
    time: 30,
    cost: {kroon: 150},
    effect: {units: {developer: 1}},
    appear_when: {tech: ["the_yledger"]},
  },

  hire_support: {
    category: "workforce",
    action_text: "Hire tech support",
    time: 30,
    cost: {kroon: 150},
    effect: {units: {support: 1}},
    appear_when: {tech: ["subordinates"]},
  },

  hire_sales: {
    category: "workforce",
    action_text: "Hire salesperson",
    time: 60,
    cost: {
      kroon: 250,
      alcohol: 1
    },
    effect: {units: {salesperson: 1}},
    appear_when: {tech: ["subordinates"]},
  },

  // hire_marketer: {
  //   category: "workforce",
  //   action_text: "Hire marketer",
  //   time: 90,
  //   cost: {
  //     kroon: 350,
  //     alcohol: 5
  //   },
  //   effect: {units: {marketer: 1}},
  //   appear_when: {tech: ["subordinates"]},
  // },

  extract_brain: {
    category: "workforce",
    action_text: "Extract developer brain",
    time: 90,
    cost: {developer: 1, kroon: 350},
    effect: {units: {manager: 1, brain: 1}},
    appear_when: {tech: ["brain_extraction"]},
  },

  // purge_manager: {
  //   category: "workforce",
  //   action_text: "Purge manager",
  //   time: 10,
  //   cost: {},
  //   effect: {units: {manager: -1}},
  //   appear_when: {minUnits: {manager: 1}},
  //   enabled_when: {minUnits: {manager: 1}},
  // },

  deploy_floatation_tank: {
    category: "workforce",
    action_text: "Deploy floatation tank",
    time: 60,
    cost: {
      kroon: 1000
    },
    effect: {units: {floatation_tank: 1}},
    appear_when: {tech: ["floatation"]},
  },

  execute: {
    category: "workforce",
    action_text: "Execute raging nerd",
    time: 60,
    cost: {kroon: 20, weapon: 1, alcohol: 1,},
    effect: {units: {raging_nerd: -1}},
    appear_when: {minUnits: {raging_nerd: 1}},
  },


  // Military

  summon: {
    category: "military",
    action_text: "Summon golem",
    time: 60 * 2,
    cost: {dump: 50, brain: 1},
    effect: {units: {golem: 1}},
    appear_when: {tech: ["summoning"]},
  },

  imbue: {
    category: "military",
    action_text: "Imbue carpet",
    time: 60 * 2,
    cost: {developer: 1, tapestry: 3, weapon: 5},
    effect: {units: {bomber: 1}},
    appear_when: {tech: ["summoning"]},
  },

  train: {
    category: "military",
    action_text: "Train soldiers",
    time: 60,
    cost: {heino: 2, crocodile: 1, back_end: 1, weapon: 2, kroon: 1000},
    effect: {units: {pro_soldier: 2}},
    appear_when: {tech: ["military", "schlager"]},
  },

  build_pro_dragon: {
    category: "military",
    action_text: "Build ProDragon",
    time: 60 * 2,
    cost: {dragon: 1, back_end: 1, weapon: 2, kroon: 5000},
    effect: {units: {pro_dragon: 1}},
    appear_when: {tech: ["military", "summoning"]},
  },

  unpersist: {
    category: "military",
    action_text: "Unpersist abomination",
    time: 60,
    cost: {developer: 1, unpersisted_achievement: 15, unpersisted_checklist_item: 15},
    effect: {units: {abomination: 1}},
    appear_when: {tech: ["summoning"],},
  },

  archon_meld: {
    category: "military",
    action_text: "Archon meld",
    time: 60,
    cost: {developer: 2},
    effect: {units: {archon: 1}},
    appear_when: {tech: ["pair_programming"],},
  },

  designate: {
    category: "military",
    action_text: "Designate team",
    time: 30,
    cost: {pro_dragon: 1, bomber: 2, abomination: 3, golem: 2, archon: 2, pro_soldier: 4},
    effect: {units: {pro_tac_team: 1}},
    appear_when: {tech: ["military", "summoning"]},
  },

  invade: {
    category: "military",
    action_text: "Invade world",
    time: 60,
    cost: {kroon: 2500, pro_tac_team: 1},
    effect: {units: {colony: 1}},
    appear_when: {tech: ["warp_gate"]},
  },


  // ethereal

  connect: {
    category: "ethereal",
    action_text: "Connect",
    time: 1,
    cost: {},
    effect: {units: {database_connection: 1}},
    appear_when: {minUnits: {back_end: 1}},
  },

  manual_dump: {
    category: "ethereal",
    action_text: "Dump",
    time: 1,
    cost: {},
    effect: {units: {dump: 1}},
    appear_when: {minUnits: {back_end: 1}, tech: ["unpersistence"]},
  },

  day_dream: {
    category: "ethereal",
    action_text: "Day dream",
    time: 1,
    cost: {},
    effect: {units: {unpersisted_achievement: 1}},
    appear_when: {minUnits: {back_end: 1}, tech: ["unpersistence"]},
  },

  deja_vu: {
    category: "ethereal",
    action_text: "Déjà vu",
    time: 1,
    cost: {},
    effect: {units: {unpersisted_checklist_item: 1}},
    appear_when: {minUnits: {back_end: 1}, tech: ["unpersistence"]},
  },

})
  .merge(opportunities)
  .merge(technologies
    .map((item, id) => fromJS({
      category: "research",
      header: item.get("name"),
      story_text: item.get("story_text"),
      action_text: "Research",
      time: item.get("time") || 0,
      cost: item.get("cost"),
      appear_when: item.get("appear_when") || {},
      visible_when: {not_all_flags: ["researched_" + id]},
      effect: {
        ...((item.get("effect") && item.get("effect").toJS()) || {}),
        flags: ["researched_" + id],
        notification: {
          sample: "/tech_jingle.ogg",
          header: "Innovation has taken place!",
          text: item.get("name") + " is now researched. " +
            (item.get("notification") || "")
        }
      }
    }))
    .mapKeys((k) => "research_" + k)
  )
  .merge(governmentTypes
    .map((item, id) => fromJS({
      category: "government",
      header: item.get("header"),
      story_text: item.get("help"),
      action_text: "Establish " + item.get("header"),
      time: item.get("time") || 60,
      cost: item.get("cost"),
      appear_when: item.get("appear_when"),
      visible_when: item.get("visible_when") || {},
      effect: {
        set_government: id,
        notification: {
          sample: "/fanfare.mp3",
          header: "New Government in Effec!",
          text: "Your company is now a " + item.get("header") + "! " +
            (item.get("notification") || "")
        }
      }
    }))
    .mapKeys((k) => "government_" + k)
  )
;
