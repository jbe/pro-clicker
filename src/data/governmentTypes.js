import {fromJS} from "immutable";

export default fromJS({
  ancient_egyptian: {
    header: "Ancient Egyptian Empire",
    help: "You rule your business like an ancient pharaoh. An effective but somewhat dated form of leadership, that provides neither benefits nor drawbacks.",
    time: 60 * 2,
    cost: { kroon: 500 },
    appear_when: { }
  },
  technocracy: {
    header: "Technocracy",
    help: "A strong AI rules on your behalf, optimizing every business decision. Code and SP production is doubled.",
    notification: "Code and SP production doubled",
    cost: { sp: 25 },
    appear_when: { tech: ["strong_ai"] }
  },
  despotism: {
    header: "Despotic Hell",
    notification: "Wages halved! This is surely the best form of government.",
    cost: {kroon: 1000},
    help: "You rule directly through violence, fear and raw display of power. Wages are cut in half, and no one dares to complain.",
    appear_when: { tech: ["despotism"] }
  },
  kingdom: {
    header: "Backwards Kingdom",
    notification: "Back end production is doubled, but at what cost?",
    help: "The king of back ends rules on your behalf. Back end production is doubled.",
    appear_when: { all_flags: ["fight_king"] }
  },
  zoo: {
    header: "Zoological Garden",
    notification: "The animals are taking over...",
    help: "Developers are encouraged to freely live out every impulse. Production of crocodiles, kolibris, pro-dragons and schlager related units are doubled. Complaint rate is doubled.",
    appear_when: { tech: ["zoo"] }
  },
  unpersisted_democracy: {
    header: "Unpersisted democracy",
    notification: "The air tingles with unmanifested possibilities.",
    help: "Developers vote on representatives that do not technically exist. Ethereal resource production is doubled.",
    appear_when: { tech: ["unpersisted_democracy"] }
  },
  post_quantum: {
    header: "Post-quantum Society",
    notification: "Wow, everything moves really fast now, but it won't help as long as we pay full wages..",
    help: "An advanced government form, where time moves twice as fast.",
    appear_when: { tech: ["quantum_government"] }
  }
});
