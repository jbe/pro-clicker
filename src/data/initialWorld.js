import { fromJS } from "immutable";
import unitTypes from "./unitTypes";
import { initialGovernment } from "data/constants";


export default fromJS({
  units: {
    counts: unitTypes.map(unit => unit.get("initial")),
    appeared: {}, // which units are visible in the ui
  },

  actions:  {
    appeared: {},
    managerAssignments: {}
  },

  production: {
    actions: {},
    workforceActions: {}
  },

  flags: {},
  appearedNewsletters: [],
  notificationLog: [],

  society: { // settings from ui
    government: initialGovernment,
    customers: 0,
    customerAcquisitionRate: 0,
    opportunities: 0,
    marketingEffect: 0,
    reputation: 0,
    complaints: 0,
    complaintRate: 0,
    opportunityRate: 0,
    rabbitHoleDepth: 0,
    drillRate: 0
  },

  environmental_bonuses: {
    golden_age: false
  },

  yledger: {
    allocations: {
      develop: 0.5,
      write: 0.5,
      squish: 0,
      ship: 0,
      assemble: 0,
      breed_crocodile: 0,
      hatch_kolibri: 0,
      create_ifen: 0,
    }
  },

});
