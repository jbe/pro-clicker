import React from 'react';
import {fromJSOrdered} from "utils";

export default fromJSOrdered({

  sp_writing: {
    name: "Deep thoughts",
    story_text: <span>
      You suddenly notice your master researcher standing in your office. You can not understand how he moves so quietly. He raises his squeaky voice: <em> "Throughout the ages, philosophers have contemplated existence. I have also tried to conduct my own ruminations, but I need more time and money. Who knows what I might find in the depths of my mind.." </em>
    </span>,

    flag_help: "Enables SP (Solution Proposal) writing.",
    notification: "Now we can write SP's!",
    time: 30,
    cost: {
      brain: 1,
      loc: 50
    },
  },

  features: {
    name: "A bold plan",
    story_text: <span>
      Your chief researcher walks into your bunker. <em> "I came up with a new theory! It might be possible to sell lines of code if we bundle them and present them as "features". But we need some more lines of code to experiment on in order to develop this idea fully." </em>
      <br/><br/>
      Pro tip: Features attract customers. Customers generate sales opportunities. Sales opportunities can be used to sell features, earning Kroons! This is the only way to make money in the game.
    </span>,
    flag_help: "Enables production of features.",
    notification: "Let's roll out new features, like big a big wet carpet!",
    time: 60,
    cost: {loc: 50, sp: 1},
    appear_when: {tech: ["sp_writing"]}
  },

  newsletter: {
    name: "Brainwashing",
    story_text: <span>
      We should establish a "Company Newsletter".
    </span>,

    flag_help: "Enables the company newsletter.",
    time: 30,
    cost: {kroon: 10},
    appear_when: {tech: ["sp_writing"]}
  },

  the_yledger: {
    name: "The Y-Ledger",
    story_text: <span>
      Our scientists are investigating a logical entity that could theoretically exist within the universe. They are calling it <em>The Developer</em>. It looks like developers could be integrated into our business through a new contraption we are calling a YLedger. We need more time and code to investigate this.
    </span>,
    flag_help: "Let's you employ workers.",
    notification: "Now you can hire developers!",
    time: 60,
    cost: {kroon: 500, sp: 1},
    appear_when: {tech: ["features", "newsletter"]}
  },

  subordinates: {
    name: "Hierarchies of subordinates",
    story_text: <span>
      I had a dream where I reigned over endless underlings, who were crushed under the weight of my supreme authority. Everything became cogs in a perfect machine. I must begin implementing this vision immediately.
    </span>,
    flag_help: "Let's you hire salespeople and support guys",
    notification: "Now you can hire support guys and sales people! However, you probably won't really get the economy going until you research despotism and become a Despotic Hell.",
    time: 60 * 2,
    cost: {
      kroon: 850,
      sp: 25
    },
    appear_when: {tech: ["the_yledger"]}
  },

  despotism: {
    name: "Despotism",
    story_text: <span>
      I need more power.<br/><br/>I should make an example of some developers to instill fear and create a proper work ethic. This carnival must come to an end. We are a serious company and not an amusement park.
      <br/><br/>
      ProTip: This is by far the best government form. You may need it to win
    </span>,
    flag_help: "Enables despotic government type, which cuts wages in half.",
    notification: "Go to the society page to instigate a revolution to be come a Despotic Hell. This will cut wages in half!",
    time: 60 * 2,
    cost: {
      developer: 5
    },
    appear_when: {tech: ["subordinates"], minUnits: {developer: 5}}
  },

  strong_ai: {
    name: "Strong AI",
    story_text: <span>
      Your master researcher suddenly stands behind you. "<em>Thank you for providing the resources I needed to develop my theories. However, now that I have had so many deep thoughts, I realize that there are some biological limitations at play in my own brain. We should develop a strong AI to be able to produce even more code and SP's.</em>"
    </span>,
    flag_help: "Enables the Technocracy government type. Technocracies produce twice as much code and SP's.",
    notification: "You can instigate a revolution from the Society page.",
    time: 60 * 2,
    cost: {
      sp: 50,
      loc: 7500,
      kroon: 1000
    },
    appear_when: {tech: ["newsletter", "debugging", "the_yledger"]}
  },

  the_back_end: {
    name: "Hyper-dense code",
    story_text: <span>
      A strange looking researcher told you about his new idea this morning: <em> "If we take lots and lots of lines of code, and compress them into a brick-like substance, the resulting material might exhibit unique attributes. The discovery of such hyper-dense materials could trigger giant leaps within all of the sciences. We are calling this emerging field of technology the Back End."</em>
    </span>,
    flag_help: "Enables back-end production.",
    notification: "Let's build some back ends now!",
    time: 60 * 2,
    cost: {
      kroon: 750,
      loc: 500,
      sp: 25
    },

    appear_when: {tech: ["subordinates"]}
  },

  unpersistence: {
    name: "Unpersistence",
    story_text: <span>
      A strange developer wiggles her way into your office, looking at you with big button-like eyes, before suddenly unleashing a tropical monsoon of meaningless words. You gather that she represents a particular guild within the developer hierarchy, whom advocate the creation of ghost-like non-entities, to watch over and somehow manipulate other abstract entities, none of which make any sense. She proposes a vast complicated machine that will create <em>unpersisted objects</em> and other unearthly intangibles.
    </span>,
    flag_help: "Back ends may produce ethereal manifestations.",
    notification: "Something has changed, the air seems different...",
    time: 60,
    cost: {
      sp: 350,
      loc: 1,
      kroon: 5000
    },
    appear_when: {tech: ["the_back_end", "brain_extraction", "strong_ai", "slavery"]}
  },

  unpersisted_democracy: {
    name: "Unpersisted beurocracy",
    story_text: <span>
      <em>
        Every revolution evaporates and leaves behind only the slime of an unpersisted bureaucracy.
      </em>
      &ndash;Franz Kafka
    </span>,
    flag_help: "Ethereal resource production is doubled.",
    notification: "You can instigate a revolution from the Society page.",
    time: 60 * 3,
    cost: {
      sp: 100,
      loc: 1,
      kroon: 5000
    },
    appear_when: {tech: ["unpersistence"]}
  },

  debugging: {
    name: "Bug squishing",
    story_text: <span>
      Your second researcher comes fumbling into your office, panting like a tired dog, covered from top to toe with bruises, scratches and blood. <em>"How can anyone work under such conditions?! We need to find some way to debug this rotting war field of an excuse for a code base, or we will all die." </em>
      <br/><br/>
      Fun fact: all complaints must first be turned into bugs before they can be removed by debugging. It's important to get rid of both as fast as possible because they lower your reputation. Low reputation scares away customers, and without customers, you won't get any sales opportunities. Without sales opportunities, you will not make any kroons.
    </span>,

    flag_help: "Enables debugging.",
    notification: "Let's squish those bugs now!",
    time: 60,
    cost: {
      kroon: 250
    },
    appear_when: {tech: ["sp_writing"], minUnits: {bug: 1}}
  },

  fermentation: {
    name: "Funny smells",
    story_text: <span>
      <em>
        "Sir, we have received reports of a strange physical process that is taking place in some piles of old documents. We are calling it fermentation. If we gained a better understanding of the process, perhaps we could turn it to our advantage?"
      </em>
    </span>,
    flag_help: "Enables fermented resources?!",
    notification: "Let's ferment some documents today!",
    time: 60,
    cost: {
      kroon: 500,
      sp: 10
    },
    appear_when: {tech: ["newsletter"], minUnits: {sp: 2}}
  },

  distillation: {
    name: "A strange developer",
    story_text: <span>
      A strange-looking developer stumbles into your office and pukes on the floor. It makes you sad, because you know you will have to execute him now, and it's so much work, so much <em>administative burden</em>. Why can't they just stand in line? You ask what the hell is wrong with him, to puke on the floor that belongs to his supreme benefactor. The developer whimpers and produces a slurred, gargling babble: <em>"I don't know what got into me.. HIC. I drank these.. HIC. condensed vapours.. HIC. That came when i boiled some fermented SP's.. HIC. It smelled funny.. HIC. Please don't kill me! HIC. I only wanted to see what would happen.."</em> The lowly developer pukes again, and you call for the guards. But perhaps this new discovery can be turned to our advantage...?
    </span>,
    flag_help: "Enables alcohol distillation. One of the many uses of alcohol is to hire sales people.",
    notification: "One of the many uses of alcohol is to hire sales people.",
    time: 60 * 2,
    cost: {
      developer: 1,
      fermented_sp: 3,
    },
    appear_when: {minUnits: {fermented_sp: 1}},
  },

  tapestries: {
    name: "The Art of the Front End",
    story_text: <span>
      A very thin and pale researcher walks into your bunker office. After having offered the customary salute, he orates: <em> "Sir, the backends have been a huge success, have they not?! I am proposing the creation of yet another exotic form of encrusted source code, called the Front End. My friends and colleagues are only laughing at me and I have had to conduct all my experiments in hiding. But soon it will be I who laugh! Will you help me in this new and exciting endeavour?!" </em>
    </span>,
    flag_help: "Enables the production of tapestries.",
    notification: "Let's make some.",
    time: 60 * 4,
    cost: {
      kroon: 5000,
      loc: 5000,
      back_end: 2,
      sp: 50
    },
    appear_when: {minUnits: {loc: 2500}}
  },

  crocodiles: {
    name: "Better Front Ends",
    story_text: <span>
      Your lead front end researcher walks into your office, draped in ornate textiles. He is actually so tightly curled up, he seems to be asphyxiating. His squeaky voice, now muffled by the cloth, forms sentences: <em> "What were we thinking!? We need better front-ends! Please fund this project, because this madness with the textiles can't go on. People are dying." </em> </span>,
    flag_help: "Enables the production of crocodiles.",
    notification: "You can make crocodiles now, sweet...",
    time: 60 * 5,
    cost: {
      kroon: 7500,
      alcohol: 30,
      tapestry: 3,
      loc: 10000
    },
    appear_when: {tech: ["tapestries"]},
  },

  kolibris: {
    name: "Even Better Front Ends",
    story_text: <span>
      You hear a familiar wooden clacking approach you. It is your lead front end researcher, humping along on his peg leg. As he limps through the door, you observe his wrinkled, contorted expression; the face of a cold, scientific man. His ornate robes and tapestries are gone. He speaks to you: <em>"After all this crocodile stuff happened, I started thinking. We've made great strides for science, but at what cost? The crocodiles may have been a mistake. Armed with the knowledge that we now have, we will create something better."</em>
    </span>,
    flag_help: "Enables the production of Kolbris, and paves way for advanced tech.",
    notification: "You can make Kolibris now. Lucky you!",
    time: 60 * 4,
    cost: {
      kroon: 20000,
      loc: 2000,
      sp: 250,
      tapestry: 25,
      crocodile: 20,
    },
    appear_when: {tech: ["crocodiles"]},
  },

  zoo: {
    name: "Zoological Garden",
    story_text: <span>
      <em>
        First I saw the white bear, then I saw the black;<br/>
        Then I saw the camel with a hump upon his back;<br/>
        Then I saw the grey wolf, with mutton in his maw;<br/>
        Then I saw the wombat waddle in the straw;<br/>
        Then I saw the elephant a &mdash; waving of his trunk;<br/>
        Then I saw the monkeys &mdash; mercy, how unpleasantly they smelt!
      </em>
      <br/><br/>
      &ndash;William Makepeace Thackeray, 1811 - 1863 
    </span>,
    flag_help: "Enables the Zoological Garden government type. Zoological gardens produce twice the amount of animals and complaints.",
    notification: "You can instigate a revolution from the Society page.",
    time: 60 * 2,
    cost: {
      developer: 5,
      crocodile: 10,
      kolibri: 20,
    },
    appear_when: {minUnits: {kolibri: 1, crocodile: 1}}
  },


  input_form_element_nests: {
    name: "InputFormElementNests",
    story_text: <span>
      Your peg-legged lead front end researcher stands in the doorway, with one bird on each shoulder. This bestows him the appearance of a pirate, weathered by the the raging storms of development. He lived to tell the tale. There is something white caked in his hair. He harks and squeaks: <em> We have a generic Kolibri component that handles form inputs, and it does a lot of weird stuff, but not enough. I propose the implementation and execution of a final solution to the form input problem. Everything is ready; we only need your go-ahead. </em>
    </span>,
    flag_help: "Enables the production of InputFormElementNests. InputFormElementNests attract dragons.",
    notification: "Make some now, and they will eventually attract Dragons!",
    time: 30,
    cost: {
      kroon: 25000,
      kolibri: 25
    },
    appear_when: {tech: ["kolibris"]},
  },

  quantum_government: {
    name: "Quantum government",
    story_text: <span>
      <em>
        The basic idea is that we create a quantum AI that rules while taking all possible outcomes into account. It does this by constructing an extremely complex quantum juxtaposition state representing a self-correcting strategy towards certain goals, based on input from quantum sensor mechanisms planted throughout the various levels of our society. This will effectively account for almost all eventualities. In effect, when happening on a large scale, it has been shown to change the perception of time.
      </em>
    </span>,
    flag_help: "Enables the post-quantum society government type. This government modifies the perception of time.",
    notification: "You will and will not instigate a revolution from the Society page. And you already have and have not.",
    time: 60 * 6,
    cost: {kroon: 100000, back_end: 50, tapestry: 50, crocodile: 50, kolibri: 50, database_connection: 5000},
    appear_when: {tech: ["unpersisted_democracy", "zoo", "despotism", "strong_ai", "biotech"]}
  },


  brain_extraction: {
    name: "Brain extraction",
    story_text: <span>
      Your chief researcher stands in the doorway. He is talking: <em>"You know, the developers can be quite smart, at least superficially. And that's good, because it enables them to write code and so on. But some times, don't you just wish for an assistant that could help you click buttons? &mdash; What if we create a machine that can extract the developer brain from the developer, leaving behind a hollow, obedient shell? We will even get some free leftover brains!"</em> The researcher winks at you as he says that.
      <br/><br/>
      Fun fact: Managers have high wages, so you should make some priorities for where you put them to work. As soon as your economy can handle a couple of managers, you should use them to establish an alcohol production line, through fermentation and destillation. Later, you might want to use managers to buy guns, and to automatically hire workers, even though this will require some supervision and manual balancing.
    </span>,
    flag_help: "Let's you create managers",
    notification: "You can now create managers! Managers click buttons so you don't have to. Your first priority should be to get a solid alcohol economy based on managers. Managers require high wages, so they should be used wisely.",
    time: 60 * 2,
    cost: { kroon: 1500 },
    appear_when: {tech: ["subordinates", "the_back_end"]}
  },

  slavery: {
    name: "An Ancient Trade",
    story_text: <span>
      A lanky man with a mustache and round glasses walks into your bunker office. <em>"Hallo, mein name ist Herr Kluff, und Ich am ze trader in ze oldest commodity known to man -- Man himself of course! Ha! Ha! -- Among many other zhings, Ich sell you only ze best developer... perhaps du interests something... different..?!  You only say to me.. ..und wir machen deine dreams come true zusammen! Ha! Ha!"</em>
    </span>,
    flag_help: "Halves developer upkeep.",
    notification: "Developer wages have been cut in half. Muhaha..",
    time: 60 * 2,
    cost: { kroon: 2500 },
    appear_when: {tech: ["subordinates", "brain_extraction"], minUnits: {developer: 5}}
  },

  ballmer: {
    name: "Plug & Play",
    story_text: <span>
      Steve Ballmer (famous Microsoft ex-CEO) walks into your underground office with a bottle in his hand. He looks at you like a raging psychotic predator. You instantly see through this masquerading street criminal. You decide to listen to him, and see how to turn things to our advantage. Ballmer says: <em> Developers.. Developers... Developers! Developers! Developers! Developers! Aaaaaaaahrgggggg! Puh... I like where you're going with this company man. If you need anything, just call me. I can sell you some Salvia or some Crocodile... Just tell me what you need. I can hook you up, give you a cable straight into the Dark Web. You know, heavy weapons, assasinations, laundering.. I have all the connections. It's plug & play, for a price &ndash, as we say at Microsoft.
      </em>

    </span>,
    flag_help: "Enables dark web trading (you can buy guns), and makes time run twice as fast when producing alcohol",
    notification: "Alcohol production is twice as fast, and we can buy guns!",
    time: 60 * 2,
    cost: { kroon: 7500, alcohol: 3 },
    appear_when: {tech: ["distillation", "slavery"]},
  },

  biotech: {
    name: "Bio-technology",
    story_text: <span>
      With all of the emerging bio-tech that has been going on recently, it is time for us to get on the bandwagon. We should expand our laboratory with a new wing, full of the best equipment and people.
    </span>,
    flag_help: "Enables advanced research",
    time: 60 * 2,
    cost: {
      kroon: 40000,
      brain: 1,
      back_end: 5,
      crocodile: 20
    },
    appear_when: {tech: ["ballmer"]},
  },

  motivator_implant: {
    name: "A very useful device",
    story_text: <span>
      Your new chief of bio-tech looks very cold and professional in his white lab coat. Standing in the doorway of your office, his glasses reflect the light like a clichéd super-villain. In his hand, he holds a bead-shaped, polished metal object. <em>"This is implant"</em>, he mutters, in his characteristic slavic accent. <em>"It is brain motivator for developer. We project three hundred percent increase in developer productivity once deploy. Need more trial, of course. Still early stage, but we make good simulation. Lazy developer will be, what you say.. ..thing of the past."</em>
    </span>,
    flag_help: "Doubles developer productivity",
    notification: "Developer productivity doubles.",
    time: 60 * 4,
    cost: {
      kroon: 35000,
      developer: 5
    },
    appear_when: {tech: ["biotech"]},
  },

  schlager: {
    name: "The Fountain of Youth",
    story_text: <span>
      Herr Kluff (the lanky slave trader with the round glasses) has offered to sell us a precious DNA sample from the German black market. Speaking in riddles, Herr Kluff only refers to the vial as <em>"the fountain of youth"</em>.
    </span>,
    flag_help: "Gain insight into a secret Germanic art.",
    notification: "You can now clone Heino's. Lucky you!",
    time: 60 * 2,
    cost: {
      kroon: 35000,
      alcohol: 100
    },
    appear_when: {tech: ["biotech", "kolibris"]},
  },

  summoning: {
    name: "Summoning",
    story_text: <span>
      <em>
        The simulacrum lifted its sleepy<br/>
        lids and saw forms and colors<br/>
        it did not understand, and lost in sounds<br/>
        it rehearsed fearful movements.<br/>
      </em>
      <br/>
      &ndash;Jorge Luis Borges, The Golem
    </span>,
    flag_help: "Enables magic military units",
    notification: "You can now summon magic military units.",
    time: 60 * 4,
    cost: {
      kroon: 10000,
      tapestry: 1,
    },
    appear_when: {tech: ["unpersisted_democracy"]},
  },

  military: {
    name: "Private company militia",
    story_text: <span>
      We need to pro-actively defend our assets. Some say turn the other cheek, but it is better to point guns.
    </span>,
    flag_help: "Enables regular military units",
    notification: "Let's acquire some soldiers as soon as possible.",
    time: 60 * 2,
    cost: {
      kroon: 10000,
    },
    appear_when: {tech: ["ballmer"]},
  },

  pair_programming: {
    name: "Pair programming",
    story_text: <span>
      Two developers combine their minds to function as a single entity of immense power.
    </span>,
    flag_help: "Enables Archon meld",
    notification: "Developers can now archon meld.",
    time: 60 * 2,
    cost: {
      sp: 100,
    },
    appear_when: {tech: ["military", "summoning"]},
  },

  warp_gate: {
    name: "Warp gate",
    story_text: <span>
      Hey ho, let's go!
    </span>,
    flag_help: "Enables faster-than-light travel to other inhabited worlds. That means colonization!",
    notification: "Let the invasion begin.",
    time: 60 * 5,
    cost: {
      kroon: 15000,
      database_connection: 2500,
      tapestry: 25
    },
    appear_when: {minUnits: {pro_tac_team: 1}},
  },

  win_game: {
    name: "Galactic empire",
    story_text: <span>
      Our empire needs a center of power; a supreme throne.
    </span>,
    flag_help: "Begins the galactic expansion. You win the game.",
    notification: "A winner is you!",
    time: 10,
    cost: {
      kroon: 35000,
      loc: 25000,
      sp: 1500,
      database_connection: 2000,
      back_end: 15,
      colony: 5
    },
    appear_when: {tech: ["warp_gate"]}
  }

});
