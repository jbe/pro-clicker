import { fromJS } from "immutable";

export const tickLength = 1 / 5; // seconds

export const initialGovernment = "ancient_egyptian";

// export const saveFilePrefix = "ProClicker_savegame_";
// export const saveFileSuffix = ".pro";
export const saveSlotLocalStorageKey = "ProClicker_save__what_are_you_doing_here_";
export const autoSaveSlotKey = "autoSaveSlot";
export const initialAutoSaveSlot = 7;
export const autoSaveFrequency = 5 * 1000;
export const saveSlotIds = fromJS([0,1,2,3,4,5,6,7]);
