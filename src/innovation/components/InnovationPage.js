import React, {Component} from 'react';
import Panel from "ui/components/Panel";
import ActionControl from "action/components/ActionControl";
import ActionTriggerPanel from "action/components/ActionTriggerPanel";
import technologies from "data/technologies";
import {
  allOpportunityActions,
  allResearchActions
} from "data/views/actionTypeViews";


class InnovationPage extends Component {
  render() {
    const {
      intermediaries,
      flags,
      units,
      trigger,
      disabledActions,
      actionProduction,
    } = this.props;

    const researchedTechs = flags
      .keySeq()
      .filter(flag => flag.indexOf("researched_") === 0)
      .map(flag => {
        const id = flag.slice(11);
        // if (!technologies.has(id)) debugger;
        return technologies.get(id);
      });


    const researchedJsx = researchedTechs.map(tech =>
        <p key={tech.get("name")} style={{ marginBottom: "2em" }}>
          {tech.get("name")}
          <br />
          <small>
            {tech.get("flag_help")}
          </small>
        </p>
      ).toJS();


    return (
      <div>
        <ActionTriggerPanel
          header="Challenges and opportunities"
          icon="flag"
          units={units}
          trigger={trigger}
          visibleActions={
            allOpportunityActions.filter((item, id) =>
              intermediaries.hasIn(["actions", "visible", id]) || false)
          }
          disabledActions={disabledActions}
          actionProduction={actionProduction}
        />

        {
          flags.has("research_enabled") &&
          allResearchActions
            .filter((item, id) => intermediaries.hasIn(["actions", "visible", id]) || false)
            .map((item, id) => {
              const {
                header,
                story_text,
                icon
              } = item.toJS();

              return (
                <Panel key={id} title={header} icon={icon}>
                  <div style={{ width: "22rem" }}>
                    <p style={{ marginBottom: "2em" }}>
                      {story_text}
                    </p>
                    <ActionControl
                      actionId={id}
                      disabled={disabledActions.hasIn([id])}
                      counts={units.get("counts")}
                      trigger={trigger}
                      production={actionProduction.get(id)}
                      progressWidth="20em"
                    />
                  </div>
                </Panel>
              );
            }).toArray()
        }
        <Panel title="Researched technologies" icon="gear">
          <hr/>
          <div style={{ width: "22rem", maxHeight: "40em", overflowY: "auto" }}>
            {
              researchedJsx.length
                ? researchedJsx
                : <p>You are not very innovative... All the cool companies do innovation. What have you come up with? Nothing, that's what.</p>
            }
          </div>
        </Panel>
      </div>
    );
  }
}

export default InnovationPage;