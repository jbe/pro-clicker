import React, { Component } from 'react';
import { Switch, Tooltip } from '@blueprintjs/core';
import Panel from "ui/components/Panel";

import unitGroups from "data/views/unitGroupViews";
/* import unitTypes from "data/unitTypes";*/

import ActionControl from "action/components/ActionControl";



export default class UnitGroupPanel extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  render() {
    const {
      visibleActions,
      disabledActions,
      unitCounts,
      groupId,
      trigger,
      actionProduction,
      managerAssignments,
      children
    } = this.props;

    /* console.log("render", groupId);*/

    // TODO: rename to actionGroupPanel..?

    if (!visibleActions.size) return null;

    const group = unitGroups.get(groupId)

    const showManagerAssignmentSwitches = managerAssignments.count(v => v) < unitCounts.get("manager");

    return (
      <Panel className="UnitGroup" title={group.get("header")}>
        {/* <UnitCountTable counts={filteredCounts} emptyMessage="No units"/> */}
        {visibleActions.size ? <hr/> : null}
        {
          visibleActions.map((action, actionId) =>
            <div key={actionId}>
              <div style={{display: "inline-block"}}>
                <ActionControl
                  key={actionId}
                  actionId={actionId}
                  disabled={disabledActions.has(actionId) || managerAssignments.get(actionId)}
                  counts={unitCounts}
                  trigger={trigger}
                  production={actionProduction.get(actionId)}
                  assignedManager={managerAssignments.get(actionId)}
                />
              </div>
              {
                (showManagerAssignmentSwitches || managerAssignments.get(actionId)) && (actionId !== "purge_manager") &&
                  <div style={{ display: "inline-block", marginLeft: "1em" }}>
                    <Tooltip
                      content="Assign manager"
                      target={
                        <Switch
                          style={{ marginTop: "0.85em" }}
                          checked={managerAssignments.get(actionId) || false}
                          onChange={() => trigger({toggle_manager_assignment: {actionId}})}
                        />
                      }
                    />
                  </div>
              }
            </div>
          ).toArray()
        }
        {children}
      </Panel>
    );
  }
}
