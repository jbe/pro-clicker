import React, { Component, Fragment } from 'react';
import { Icon, Tooltip, NavbarDivider } from "@blueprintjs/core";
import unitTypes from "data/unitTypes";
import unitGroupViews from "data/views/unitGroupViews";

class UnitCountIcon extends Component {
  render() {
    const {
      showHelp,
      count,
      unitId,
      alwaysShow,
      precision
    } = this.props;

    const {
      icon,
      singular,
      help
    } = unitTypes.get(unitId).toJS();

    if (!count && !alwaysShow) return null;

    const style = count ? {} : {color: "grey"};

    const tipJsx = showHelp 
      ? <span><strong>{singular}</strong>: {help}</span>
      : <span>{singular}</span>;

    const iconAndCountJsx = (
      <span style={{ paddingLeft: "0.6em", ...style }}>
        <Icon icon={icon} title={singular}/>
        <span style={{ paddingLeft: "0.3em", style }}>
          { count.toFixed(precision || 0) }
        </span>
      </span>
    );

    return (
      <Tooltip
        content={tipJsx}
        target={iconAndCountJsx}
      />
    );
  }
}

export default class UnitCountIcons extends Component {
  render() {
    const {
      showHelp,
      counts,
      alwaysShowUnits,
      precision,
      noDivider
    } = this.props;

    if (!counts) return null;

    const jsx = [];

    unitGroupViews.forEach((v, group_id) => {
      let first = true;
      v.get("units").forEach((unit, unitId) => {
        const alwaysShow = alwaysShowUnits && alwaysShowUnits.get(unitId);
        if (!noDivider && first && (alwaysShow || v.get("units").filter((u, k) => counts.get(k)).size))
          jsx.push(<NavbarDivider key={unitId + "-divider"}/>);
        first = false;
        jsx.push(
          <UnitCountIcon
            showHelp={showHelp}
            key={unitId}
            unitId={unitId}
            precision={precision}
            alwaysShow={alwaysShow}
            count={counts.get(unitId)}
          />
        );
      });
    });

    return (
      <Fragment>
        { jsx }
      </Fragment>
    );
  }
}
