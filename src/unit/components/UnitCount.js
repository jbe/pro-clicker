import React, {Component} from 'react';
import unitTypes from "data/unitTypes";

export default class UnitCount extends Component {
  render() {
    const {
      unitId,
      count,
      hideNumber
    } = this.props;

    const unitType = unitTypes.get(unitId);

    const {
      singular,
      plural
    } = unitType.toJS();

    return (
      <span>
        {hideNumber ? null : count}
        {" "}
        {Math.abs(count) === 1 ? singular : plural}
      </span>
    );
  }
}
