import React, {Component} from 'react';
import UnitCountRow from "unit/components/UnitCountRow";

export default class UnitCountTable extends Component {
  render() {
    const {
      counts,
      availableCounts,
      noUnitName,
      emptyMessage
    } = this.props;

    if (!counts.size || (counts.size === 1 && counts.has("time") && !counts.get("time"))) return (
      <span>{emptyMessage || "Free as the wind."}</span>
    );


    return (
      <table>
        <tbody>
          {
            counts.map((count, unitId) =>
              <UnitCountRow
                key={unitId}
                unitId={unitId}
                count={count}
                availableCount={availableCounts && availableCounts.get(unitId)}
                noUnitName={noUnitName}
              />
            ).toArray()
          }
        </tbody>
      </table>
    );
  }
}
