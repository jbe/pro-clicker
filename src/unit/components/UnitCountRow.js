import React, {Component} from 'react';
import { Icon, Tooltip } from "@blueprintjs/core";
import unitTypes from "data/unitTypes";
import {
  humanizeSecs
} from "utils";
import UnitCount from "unit/components/UnitCount";

export default class UnitCountRow extends Component {
  render() {
    const {
      noUnitName,
      count,
      unitId,
      availableCount
    } = this.props;

    const unavailable = (availableCount !== undefined) && (availableCount < count);

    let labelStyle = null;

    if (unavailable) labelStyle = {color: "#faa"};
    else if (!count) labelStyle = {color: "grey"};

    if (unitId === "time") {
      const timestr = humanizeSecs(count).split(" ");

      return (
        <tr>
          <td style={labelStyle}>
            <Icon icon="time" title="Time"/>
          </td>
          <td style={{...labelStyle, paddingLeft: "1em"}}>
            {timestr[0]}
            {noUnitName ? timestr.slice(1).join(" ") : null}
          </td>
          {
            noUnitName ? null : (
              <td style={{...labelStyle}}>
                {timestr.slice(1).join(" ")}
              </td>
            )
          }
        </tr>
      );
    }

    const {
      icon,
      singular,
      help
    } = unitTypes.get(unitId).toJS();

    const nameRowJsx = noUnitName ? null : (
      <td style={labelStyle}>
          <Tooltip
            content={help}
            target={
              <span>
                <UnitCount hideNumber unitId={unitId} count={count}/>
                { unavailable ? <span> ({Math.floor(count - availableCount)})</span> : null }
              </span>
            }
          />
      </td>
    );

    return (
      <tr>
        <td style={labelStyle}>
          <Icon icon={icon} title={singular}/>
        </td>
        <td style={{...labelStyle, paddingLeft: "1em"}}>
          { count }
        </td>
        { nameRowJsx }
      </tr>
    );
  }
}
