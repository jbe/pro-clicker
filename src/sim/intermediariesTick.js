import { fromJS } from "immutable";
/* import { tickLength } from "data/constants";*/
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";
import { checkCondition } from "sim/conditions";
import { addMapValues } from "utils";

const workforceUnitTypes = unitTypes
  .filter(unit => unit.has("actionContributions"));

const allocateableWorkforceUnitTypes = workforceUnitTypes
  .filter(unit => unit.has("contributionAllocationStatePath"));


function normalizeWeights(weights) {
  const totalWeight = weights.filter(a => a).reduce((a, b) => a + b);
  const normalizedWeights = weights.map(weight => weight / totalWeight);
  return normalizedWeights;
}


export default function computeTickIntermediaries(world) {
  // world = world.deleteIn(["actions", "appeared", "todo"]);

  // Compute modifiers
  
  const currentGovernment = world.getIn(["society", "government"]);

  const devProdMult = checkCondition(world, {tech: ["motivator_implant"]}) ? 2 : 1;

  const ballmerMult = checkCondition(world, {tech: ["ballmer"]}) ? 2 : 1;

  const modifiers = {
    wagesMultiplier: currentGovernment === "despotism" ? 0.5 : 1,
    unitWagesMultiplier: {
      developer: checkCondition(world, {tech: ["slavery"]}) ? 0.5 : 1
    },

    complaintMultiplier: currentGovernment === "zoo" ? 2 : 1,

    actionProductionMultipliers: {
      // these are also hard-coded in operations.js (used by effects)
      develop: (currentGovernment === "technocracy" ? 2 : 1) * devProdMult,
      write: (currentGovernment === "technocracy" ? 2 : 1) * devProdMult,
      assemble: (currentGovernment === "kingdom" ? 2 : 1) * devProdMult,
      weave: devProdMult,
      ship: devProdMult,
      squish: devProdMult,
      distill: ballmerMult,
      ferment: ballmerMult,

      breed_crocodile: (currentGovernment === "zoo" ? 2 : 1) * devProdMult,
      hatch_kolibri: (currentGovernment === "zoo" ? 2 : 1) * devProdMult,
      create_ifen: devProdMult,
  
      // clone_heino: currentGovernment === "zoo" ? 2 : 1,
      // build_pro_dragon: currentGovernment === "zoo" ? 2 : 1,

      day_dream: (currentGovernment === "unpersisted_democracy" ? 2 : 1),
      deja_vu: (currentGovernment === "unpersisted_democracy" ? 2 : 1),
    }
  };


  // Compute action visibility

  const appearedActions = world.getIn(["actions", "appeared"]);

  const visibleActions  = appearedActions.filter((_, id) => {
    const item = actionTypes.get(id);
    if (!item.has("visible_when")) return true;

    return checkCondition(world, item.get("visible_when"));
  });

  const disabledActions = actionTypes
    .filter((item, id) =>
      item.has("enabled_when") && !checkCondition(world, item.get("enabled_when")))
    .map(() => true);



  // Compute upkeep

  const upkeepSum = unitTypes
    .filter(unit => unit.has("upkeep"))
    .map((unit, unitId) =>
      unit.get("upkeep")
          .map(cost =>
            cost * -world.getIn(["units", "counts", unitId])
            * modifiers.wagesMultiplier
            * (modifiers.unitWagesMultiplier[unitId] || 1)
          ))
    .reduce(addMapValues, fromJS({}));


  // Normalize workforce allocations

  // TODO: here be bugs!

  const normalizedWorkforceAllocations = allocateableWorkforceUnitTypes.map(unitType =>
    normalizeWeights(
      world.getIn(unitType.get("contributionAllocationStatePath"))
    )
  );

  const workforceProductionCapacities = workforceUnitTypes.map((unitType, unitId) => {
    const contributedUnits = unitType
      .get("actionContributions")
      .map((allocation, actionId) => {

        return addMapValues(
          actionTypes.getIn([actionId, "effect", "units"]),
          actionTypes.getIn([actionId, "cost"]).map(count => -count)
        )
          .map((prodCount, prodUnitId) => {
            const allocateable = normalizedWorkforceAllocations.has(unitId);
            const allocation = allocateable && (normalizedWorkforceAllocations.getIn([unitId, actionId]) || 0);
            const maxProductionPerSecond = prodCount / actionTypes.getIn([actionId, "time"]);
            const allocatedProductionPerSecond = allocateable ? maxProductionPerSecond * allocation : maxProductionPerSecond;
            return allocatedProductionPerSecond * 
              (modifiers.actionProductionMultipliers[prodUnitId]
                ? modifiers.actionProductionMultipliers[prodUnitId] : 1);
          })
      }
      );

    return contributedUnits
      .reduce(addMapValues)
      .map((prodCount, producedUnitId) => prodCount * world.getIn(["units", "counts", unitId]))
  });

  const workforceProductionCapacitySum = workforceProductionCapacities.reduce(addMapValues);

  return fromJS({
    modifiers,
    actions: {
      visible: visibleActions,
      disabled: disabledActions
    },
    yledger: {
      normalizedWorkforceAllocations,
      upkeepSum,
      workforceProductionCapacities,
      workforceProductionCapacitySum
    }
  });
}
