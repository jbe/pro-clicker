import PropTypes from "prop-types";

export function hasMinUnits(actualCounts, queryCounts) {
  if (actualCounts.toJS) actualCounts = actualCounts.toJS();
  if (queryCounts && queryCounts.toJS) queryCounts = queryCounts.toJS();

  for (let k in queryCounts)
    if (queryCounts.hasOwnProperty(k))
      if (!actualCounts.hasOwnProperty(k) || (actualCounts[k] < queryCounts[k])) return false;
  return true;
}

export function hasAllFlags(actualFlags, wantedFlags, invert) {
  if (actualFlags.toJS) actualFlags = actualFlags.toJS();
  if (wantedFlags && wantedFlags.toJS) wantedFlags = wantedFlags.toJS();

  // if (actualFlags.code_trap && wantedFlags && wantedFlags.length) debugger;

  for (let k in wantedFlags)
  {
    // if (wantedFlags[k] === "never") debugger;
    if (!actualFlags[wantedFlags[k]]) return !!invert;
  }
  return !invert;
}

export function hasAnyFlags(actualFlags, wantedFlags, invert) {
  if (actualFlags.toJS) actualFlags = actualFlags.toJS();
  if (wantedFlags && wantedFlags.toJS) wantedFlags = wantedFlags.toJS();
  for (let k in wantedFlags)
    if (actualFlags[wantedFlags[k]]) return !invert;
  return invert;
}

export function notHasAllFlags(actualFlags, notWantedFlags) {
  return hasAllFlags(actualFlags, notWantedFlags, true);
}

export function notHasAnyFlags(actualFlags, notWantedFlags) {
  return hasAnyFlags(actualFlags, notWantedFlags, true);
}

export function hasTech(flags, wantedTechs) {
  if (!wantedTechs) return true;
  return hasAllFlags(flags, wantedTechs.map(id => "researched_" + id));
}

export function hasGovernment(society, wantedGov) {
  if (!wantedGov) return true;
  return society.get("government") === wantedGov;
}

export function notHasGovernment(society, wantedGov) {
  if (!wantedGov) return true;
  return society.get("government") !== wantedGov;
}

const conditionPropType = {
  minUnits: PropTypes.objectOf(PropTypes.number),
  tech: PropTypes.arrayOf(PropTypes.string)
};


export function checkCondition(world, condition) {
  if (condition.toJS) condition = condition.toJS();

  PropTypes.checkPropTypes(conditionPropType, condition, "prop", "checkCondition");

  // if (condition.minUnits && condition.minUnits.tech) debugger;

  return true
    && hasMinUnits(world.getIn(["units", "counts"]), condition.minUnits)
    && hasTech(world.getIn(["flags"]), condition.tech)
    && hasAllFlags(world.get("flags"), condition.all_flags)
    && (!condition.not_all_flags || notHasAllFlags(world.get("flags"), condition.not_all_flags))
    && (!condition.any_flags     || hasAnyFlags(world.get("flags"), condition.any_flags))
    && (!condition.not_any_flags || notHasAnyFlags(world.get("flags"), condition.not_any_flags))
    && hasGovernment(world.get("society"), condition.government)
    && notHasGovernment(world.get("society"), condition.government_not)
  ;
}
