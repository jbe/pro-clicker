import { fromJS, Map } from "immutable";
import actionTypes from "data/actionTypes";
import { hasMinUnits } from "sim/conditions";
import governmentTypes from "data/governmentTypes";


// Units

export function addUnits(world, add) {

  // const currentGovernment = world.getIn(["society", "government"]);

  const newCounts = world.getIn(["units", "counts"])
        .map((count, id) =>
             add.hasOwnProperty(id) ? count + add[id] : count);

  return world.setIn(["units", "counts"], newCounts);
}

export function subtractUnits(world, subtract) {
  const newCounts = world.getIn(["units", "counts"])
        .map((count, id) =>
             subtract.hasOwnProperty(id) ? count - subtract[id] : count);

  return world.setIn(["units", "counts"], newCounts);
}

export function setFlags(world, flags) {
  return world.mergeIn(["flags"], Map(flags.map(f => [f, true])));
}

// Actions

function queueAction(world, id, time) {
  return world.setIn(["production", "actions", id], fromJS({
    actionId: id,
    elapsed: 0,
    total: time
  }));
}

export function triggerAction(world, id) {
  const action = actionTypes.get(id);

  const {
    cost,
    time
  } = action.toJS();

  const safeCost = cost || {};

  const hasEnough = hasMinUnits(world.getIn(["units", "counts"]), safeCost);

  world = queueAction(world, id, time);

  if (hasEnough) {
    world = subtractUnits(world, safeCost);
  }
  else {
    console.log("Not enough units! Technically a bug, but not necessarily serious.", {world, id, action});
    console.trace();
    // throw new Error("not enough units!");
  }

  return world;
}

// Society

export function setGovernment(world, government) {

  if (!governmentTypes.has(government))
    throw new Error("Invalid government: " + government);

  return world.setIn(["society", "government"], fromJS(government));
}

export function setAllocation(world, {id, value}) {
  // console.log(world.setIn(["yledger", "allocations", id], value));
  return world.setIn(["yledger", "allocations", id], value);
}

export function pushNotification(world, {header, text, sample}) {
  return world.updateIn(["notifications"], notifications =>
    (notifications || fromJS([])).push(fromJS({
      header, 
      text,
      sample: sample || "/notification.mp3",
      time: (new Date()).getTime()
    }))
  )
    .set("notificationLog", world.get("notificationLog").push({header, text}))
}

export function toggleManagerAssignment(world, {actionId}) {
  return world.setIn(["actions", "managerAssignments", actionId],
    !world.getIn(["actions", "managerAssignments", actionId]));
}