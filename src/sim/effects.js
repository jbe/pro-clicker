import PropTypes from "prop-types";
import {
  addUnits,
  setFlags,
  triggerAction,
  setGovernment,
  setAllocation,
  pushNotification,
  toggleManagerAssignment
} from "sim/operations";

const effectPropType = {
  units: PropTypes.objectOf(PropTypes.number),
  tech: PropTypes.arrayOf(PropTypes.string)
};

export function applyEffect(world, effect) {
  PropTypes.checkPropTypes(effectPropType, effect, "prop", "applyEffect");

  // accept both immutable and js effects
  if (effect.toJS) effect = effect.toJS();

  // console.log("Effect:", effect);

  if (effect.trigger_actions)
    world = effect.trigger_actions.reduce((w, a) => triggerAction(w, a), world);

  if (effect.units) world = addUnits(world, effect.units);

  if (effect.flags) world = setFlags(world, effect.flags);

  if (effect.notification) world = pushNotification(world, effect.notification);

  if (effect.set_government) world = setGovernment(world, effect.set_government);

  if (effect.set_allocation) world = setAllocation(world, effect.set_allocation);

  if (effect.toggle_manager_assignment) world = toggleManagerAssignment(world, effect.toggle_manager_assignment);

  return world;
  /* return effectProcessors.reduce((w, processor) => processor(w, effect, applyEffects), world);*/
}

export function applyEffects(world, effects) {
  return effects.reduce(applyEffect, world);
}
