import { tickLength } from "data/constants";
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";
import newsletters from "data/newsletters";
import { applyEffect, applyEffects } from "sim/effects";
import { checkCondition } from "sim/conditions";
import { fromJS, List } from "immutable";
import { addMapValues } from "utils";


function actionProductionTick(world, intermediaries) {
  const finishedActions = world.getIn(["production", "actions"])
        .filter(item => item.get("elapsed") >= item.get("total"))
        .map(item => actionTypes.get(item.get("actionId")));

  const researchSpeed = world.getIn(["environmental_bonuses", "golden_age"]) ? 2 : 1;

  world = world.updateIn(
    ["production", "actions"],
    (items) => items
      .filter((item, id) => !finishedActions.has(id))
      .map((item, id) => {
        const speed = actionTypes.getIn(["id", "category"]) === "research" ? researchSpeed : 1;
        return item.set("elapsed", item.get("elapsed") + tickLength * speed * (intermediaries.getIn(["modifiers", "actionProductionMultipliers", id]) || 1));
      })
  );

  world = applyEffects(world, finishedActions.map((a) => a.get("effect")));

  return world;
}

function computeAppeared(world, data, appeared) {
  return data.filter((item, id) => {
    if (appeared.has(id)) return true;
    if (item.has("appear_when")) {
      // if (item.get("category") === "government") debugger;
      return checkCondition(world, item.get("appear_when"));
    }
    return true;
  });
}

function visibilityStuffTick(world) {
  const appearedUnits   = computeAppeared(world, unitTypes,   
    world.getIn(["units",   "appeared"]));
  const appearedActions = computeAppeared(world, actionTypes, 
    world.getIn(["actions", "appeared"]));

  const notificationEffects = [];

  if (world.getIn(["flags", "intro_closed"])) {
    appearedActions
      .filter((action, id) => 
        action.get("category") === "research"
        && !world.hasIn(["actions", "appeared", id]))
      .forEach(action =>
        notificationEffects.push({
          notification: {
            sample: "gongding.mp3",
            header: "New innovation project available",
            text: '"' + action.get("header") + "\" is now available for research."
          }
        })
      );
    appearedActions
      .filter((action, id) => 
        action.get("category") === "opportunity"
        && !world.hasIn(["actions", "appeared", id]))
      .forEach(action =>
        notificationEffects.push({
          notification: {
            sample: "gongding.mp3",
            header: "New opportunity!",
            text: "There is a new opportunity waiting for you on the innovation page!"
          }
        })
      );
  }

  world = applyEffects(world, notificationEffects);

  /* console.log({visibleActions, flags: world.get("flags")}); */

  return world
    .setIn(["units",   "appeared"], appearedUnits.map(item => true))
    .setIn(["actions", "appeared"], appearedActions.map(item => true));
}

function societyTick(world, intermediaries) {
  const sideEffects = [];

  const unitCounts = world.getIn(["units", "counts"]).toJS();
  const society = world.getIn(["society"]);

  const customers = society.get("customers");
  const opportunities = society.get("opportunities");
  const rabbitHoleDepth = society.get("rabbitHoleDepth");
  const complaints = society.get("complaints");
  // const salesWorkPerformed = society.get("salesWorkPerformed") || 0;

  const problems = unitCounts.bug + unitCounts.complaint;
  const marketingEffect = unitCounts.marketer * 2;
  const reputation = marketingEffect + Math.sqrt(unitCounts.feature) + Math.sqrt(unitCounts.heino) * 0.5 - problems;

  const customerAcquisitionRate = (reputation * 0.008 - customers * 0.001);

  const complaintRate = 
       customers * 0.005 + Math.min(0, customerAcquisitionRate)
       * intermediaries.getIn(["modifiers", "complaintMultiplier"]);
  const opportunityRate = (customers * 0.01 + unitCounts.salesperson * 0.01 + unitCounts.marketer * 0.02);
  // const salesWorkRate = unitCounts.marketer * (1/actionTypes.getIn(["sell", "time"]))
  const drillRate = 0;

  const newCustomers       = Math.max(customers + customerAcquisitionRate * tickLength, 0);
  const newComplaints      = Math.max(complaints + complaintRate * tickLength, 0);
  const newOpportunities   = Math.max(opportunities + opportunityRate * tickLength, 0);
  const newRabbitHoleDepth = rabbitHoleDepth + drillRate * tickLength;
  // const newSalesWorkPerformed   = salesWorkPerformed + salesWorkRate * tickLength;

  const wholeComplaints = Math.floor(newComplaints);
  if (wholeComplaints) sideEffects.push({units: {complaint: wholeComplaints}});

  const wholeOpportunities = Math.floor(newOpportunities);
  if (wholeOpportunities) sideEffects.push({units: {opportunity: wholeOpportunities}});

  // const wholeSalesWorkPerformed = Math.floor(newSalesWorkPerformed);

  world = world.mergeIn(["society"], fromJS({
    problems,
    marketingEffect,
    reputation,
    customerAcquisitionRate,
    complaintRate,
    opportunityRate,
    drillRate,
    customers: newCustomers,
    complaints: newComplaints - wholeComplaints,
    opportunities: newOpportunities - wholeOpportunities,
    rabbitHoleDepth: newRabbitHoleDepth
  }))

  world = applyEffects(world, sideEffects);

  return world;
}

function environmentalBonusesTick(world, intermediaries) {
  const oldBonuses = world.get("environmental_bonuses");
  const newBonuses = oldBonuses.toJS();

  let effects = [];

  if (!oldBonuses.get("golden_age") && world.getIn(["society", "reputation"]) > 10)
  {
    newBonuses.golden_age = true;
    effects.push({
      notification: {
        sample: "/clapclap.mp3",
        header: "Golden age begun",
        text: "Your company has entered a golden age! Research speed is doubled!"
      }
    });
  }

  if (oldBonuses.get("golden_age") && world.getIn(["society", "reputation"]) < 5)
  {
    newBonuses.golden_age = false;
    effects.push({
      notification: {
        sample: "/papercrumple.ogg",
        header: "Golden age ended",
        text: "The golden age has ended."
      }
    });
  }

  world = world.set("environmental_bonuses", fromJS(newBonuses));
  world = applyEffects(world, effects);

  return world;
}

function yledgerTick(world, intermediaries) {

  const weightedAllocations = intermediaries
    .getIn(["yledger", "normalizedWorkforceAllocations"])
    .map((unit_allocations, unit_id) => 
      unit_allocations.map(v => (v||0) * world.getIn(["units", "counts", unit_id]))
    );

  const nonWeightedAllocations = unitTypes
    .filter(unit =>
      unit.has("actionContributions") 
      && !unit.get("contributionAllocationStatePath"))
    .map((unit, unit_id) => 
      unit.getIn(["actionContributions"])
        .map(v => (v||0) * world.getIn(["units", "counts", unit_id]))
    );

  const allocations = weightedAllocations
    .merge(nonWeightedAllocations)
    .reduce((reduction, v, k) => {
      return addMapValues(reduction, v)
    }, fromJS({}));

  const prod = allocations.map(() => 0)
    .merge(world.getIn(["production", "workforceActions"]) || fromJS({}));

  let effects = [];

  // compute the new progress of each unit action being produced, while producing side effects
  const newProd = prod
    .filter((progress, action_id) => {
      const enabled_when = actionTypes.getIn([action_id, "enabled_when"]);
      const visible_when = actionTypes.getIn([action_id, "visible_when"]);
      const appeared = world.getIn(["actions", "appeared"]);

      return ((!enabled_when || checkCondition(world, enabled_when))
        && (!visible_when || checkCondition(world, visible_when)))
        && appeared.has(action_id);
    })
    .map((progress, action_id) => {
    const allocated = allocations.get(action_id);
    const fullActionCost = actionTypes.getIn([action_id, "cost"]);
    const actionTimeCost = actionTypes.getIn([action_id, "time"]);
    const productiveTimePerTick = tickLength * allocated * (intermediaries.getIn(["modifiers", "actionProductionMultipliers", action_id]) || 1);

    const costOfTick = fullActionCost.map((cost, action_id) => cost * productiveTimePerTick / actionTimeCost);

    // if we have plenty resources and action is enabled, spend resources and increment by dt
    if (checkCondition(world, {minUnits: costOfTick.toJS()})) {
      effects.push({units: costOfTick.map(v => -v).toJS()});
      const newProgress = progress + productiveTimePerTick;

      // produce the unit if we are finished now
      if (newProgress >= actionTimeCost) {
        const timesPerformed = Math.floor(newProgress / actionTimeCost);
        for (let i=0; i<timesPerformed; i++)
          effects.push(actionTypes.getIn([action_id, "effect"]));
        return newProgress - actionTimeCost * timesPerformed;
      }

      return newProgress;
    }
    return progress;
  });

  effects.push({
    units: intermediaries.getIn(["yledger", "upkeepSum"]).toJS()
  });

  world = applyEffects(world, effects);

  world = world.setIn(["production", "workforceActions"], newProd);



  // handle upkeep deficits by getting rid of units,
  // in exchange for kroon payout

  effects = [];
  if (world.getIn(["units", "counts", "kroon"]) < 0)
  {
    world = world.setIn(["units", "counts", "kroon"], 0);

    const unitWithUpkeep = unitTypes.findKey((unit, id) => unit.get("upkeep")
      && world.getIn(["units", "counts", id]));

    effects.push({
      units: {
        [unitWithUpkeep]: -1,
        kroon:  75
      },
      notification: {
        intent: "danger",
        sample: "/boo.ogg",
        header: "Kroon deficiency",
        text: "You have run out of kroons and are unable to pay upkeep! You are forced to get rid of one " + unitTypes.get(unitWithUpkeep).get("singular") + " earning you back a lousy 75 kroons!"
      }
    });
  }
  world = applyEffects(world, effects);

  return world;
}


function managerAssignmentsTick(world, intermediaries) {

  const assignments = world
    .getIn(["actions", "managerAssignments"])
    .filter((assigned, actionId) => {
      if (!assigned) return false;
      if (intermediaries.getIn(["actions", "disabled", actionId])) return false;
      if (world.getIn(["production", "actions", actionId])) return false;

      if (!checkCondition(world, {
        minUnits: actionTypes.getIn([actionId, "cost"]).toJS()
      })) return false;

      return true;
    });

  assignments.forEach((_, actionId) => {
    world = applyEffect(world, {
      trigger_actions: [actionId]
    });
  });

  // while there are more assignments than managers, remove assignments
  while (world.getIn(["actions", "managerAssignments"]).filter(a => a).size > world.getIn(["units", "counts", "manager"]))
    world = world.deleteIn(
      ["actions", "managerAssignments",
        world.getIn(["actions", "managerAssignments"]).keySeq().first()
      ]
    );

  return world;
}

function backendEffectsTick(world, intermediaries) {
  const counts = world.getIn(["units", "counts"])

  if (Math.random() / tickLength < 0.2) {
    const dbConnectionsProduced = Math.floor(Math.random() * counts.get("back_end"));

    return applyEffect(world, {
      units: {
        database_connection: dbConnectionsProduced
      }
    });
  }

  return world;
}

function ifenEffectsTick(world, intermediaries) {
  
  const counts = world.getIn(["units", "counts"])

  if ((counts.get("ifen") > counts.get("dragon")) && (Math.random() / tickLength < 0.05)) {
    const dragonsProduced = Math.floor(Math.random() * counts.get("ifen") * 0.2);

    return applyEffect(world, {
      units: {
        dragon: dragonsProduced
      }
    });
  }
  
  return world;
}

function newsletterTick(world, intermediaries) {

  const newsletterEnabled = checkCondition(world, {tech: ["newsletter"]});
  if (!newsletterEnabled) return world;

  const alreadyAppearedNewsletters = world.getIn(["appearedNewsletters"]) || fromJS([]);

  const newNewsletters = List(newsletters
    .filter((newsletter, id) => {
      const cond = newsletter.get("trigger");
      return cond && checkCondition(world, cond.toJS()) && !alreadyAppearedNewsletters.includes(id);
    }).keys());

  const effects = [];

  if (newNewsletters.size) {
    effects.push({
      notification: {
        header: "New newsletter is out!",
        text: "Make sure to read it and familiarize yourself with all the points."
      }
    })
  }

  world = applyEffects(world, effects);
  world = world.setIn(["appearedNewsletters"], newNewsletters.concat(alreadyAppearedNewsletters));

  return world;
}

export default function worldTick(world, intermediaries) {

  world = actionProductionTick(world, intermediaries);
  world = visibilityStuffTick(world);
  world = societyTick(world, intermediaries);
  world = environmentalBonusesTick(world, intermediaries); // TODO: should probably be in modifiers
  world = yledgerTick(world, intermediaries);
  world = managerAssignmentsTick(world, intermediaries);
  world = backendEffectsTick(world, intermediaries);
  world = ifenEffectsTick(world, intermediaries);
  world = newsletterTick(world, intermediaries);

  // prune old notifications

  world = world.updateIn(["notifications"], (notifications) =>
    (notifications || fromJS([])).filter(notification => {
      // debugger;
      const diff = (new Date()).getTime() - notification.get("time");
      return diff < 20000;
    })
  );

  return world;
}
