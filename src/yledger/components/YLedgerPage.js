import React, { Component, Fragment } from 'react';
import { fromJS } from "immutable";
import Panel from "ui/components/Panel";
import UnitCountIcons from "unit/components/UnitCountIcons";
import UnitCountTable from "unit/components/UnitCountTable";
import EffectTable from "effect/components/EffectTable";
import { Slider, Spinner, Tooltip, Icon } from "@blueprintjs/core";
/* import { tickLength } from "data/constants";*/
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";

const ledgerableUnitTypes = unitTypes
  .filter(unit => unit.has("upkeep") || unit.has("actionContributions"));

// TODO: move to own file, also use in ActionControl
const ActionCostEffect = ({ actionId, unitCounts }) => (
  <div style={{ padding: "1em" }}>
    <p><strong>Cost:</strong></p>
    <UnitCountTable availableCounts={unitCounts} counts={actionTypes.getIn([actionId, "cost"]).set("time", actionTypes.getIn([actionId, "time"]))} />
    <p style={{ marginTop: "2em" }}><strong>Effect:</strong></p>
    <EffectTable effect={actionTypes.getIn([actionId, "effect"])}/>
  </div>
);

class AllocationSliderRow extends Component {
  render() {
    const {
      trigger,
      appearedActions,
      allocations,
      label,
      id,
      unitCounts
    } = this.props;

    if (!appearedActions.get(id)) return null;

    return (
      <tr className="AllocationSlider">
        <td>
          <Tooltip
            content={
              <Fragment>
                <ActionCostEffect actionId={id} unitCounts={unitCounts} />
              </Fragment>
            }
            target={
              <Fragment>
                {label}
              </Fragment>
            }
          />
        </td>

        <td style={{ paddingLeft: "2em" }}>
          <Slider
            stepSize={0.01}
            min={0}
            max={1}
            value={allocations.get(id)}
            onChange={(value) => trigger({ set_allocation: { id, value } })}
          />
        </td>
      </tr>
    );
  }
}

export default class YLedgerPanel extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  shouldComponentUpdate(newProps) {
    if (newProps.yledger !== this.props.yledger) return true;
    if (newProps.yledgerIntermediaries !== this.props.yledgerIntermediaries) return true;
    return false;
  }

  render() {

    const {
      yledgerIntermediaries,
      unitCounts,
      yledger,
      trigger,
      appearedActions,
      workforceActionProduction
    } = this.props;

    /* if (!yledgerIntermediaries.get("rows").size) return (
     *   <Panel className="YLedgerPanel" title="Y-Ledger">
     *     <p>
     *       Y-Ledger is empty. Go eat waffles.
     *     </p>
     *   </Panel>
     * );*/

    const progresses = workforceActionProduction
      .map((v, id) => v / actionTypes.getIn([id, "time"]))   ;

    const allocationSlidersMeta = [
      {
        label: "Develop code",
        id: "develop"
      },
      {
        label: "Write SP's",
        id: "write"
      },
      {
        label: "Squish bugs",
        id: "squish"
      },
      {
        label: "Ship features",
        id: "ship"
      },
      {
        label: "Assemble back ends",
        id: "assemble"
      },
      {
        label: "Weave tapestries",
        id: "weave"
      },
      {
        label: "Breed crocodiles",
        id: "breed_crocodile"
      },
      {
        label: "Hatch Kolibris",
        id: "hatch_kolibri"
      },
      {
        label: "Create InputFormElementNests",
        id: "create_ifen"
      },
    ];

    const workforce = ledgerableUnitTypes.filter((unit, id) => unitCounts.get(id))

    if (!workforce.size) return <Panel className="YLedgerPanel" title="Y-Ledger" icon="timeline-bar-chart">
      <hr />
      <p>
        You have not yet acquired any workers.
      </p>
    </Panel>;

    return (
      <Fragment>

        <Panel className="YLedgerPanel" title="Y-Ledger" icon="timeline-bar-chart" width="53em">
          <table className="pt-html-table pt-html-table-bordered">
            <tbody>
              <tr>
                <th style={{ textAlign: "right" }}></th>
                <th>Upkeep/s</th>
                <th>Theoretical cumulative/s</th>
                <th>Production</th>
              </tr>
              {
                workforce.map((unit, id) => {
                  return (
                    <tr key={id}>
                      <th style={{ textAlign: "right" }}>
                        {unit.get("plural")}
                      </th>
                      <td style={{width: "6em"}}>
                        <UnitCountIcons noDivider precision={1} counts={(unit.get("upkeep")||fromJS({})).map(v => -v * unitCounts.get(id))} />
                      </td>
                      <td style={{width: "30em"}}>
                        <UnitCountIcons noDivider precision={2} counts={yledgerIntermediaries.getIn(["workforceProductionCapacities", id])} />
                      </td>
                      <td style={{width: "12em"}}>
                        {
                          unitTypes.hasIn([id, "actionContributions"]) 
                          && unitTypes.getIn([id, "actionContributions"])
                            .keySeq()
                            .filter((action_id) => progresses.has(action_id))
                            .map(action_id => {

                              const firstUnitEffect = actionTypes.getIn([action_id, "effect", "units"]).keySeq().first(0);
                              const icon = unitTypes.getIn([firstUnitEffect, "icon"]);

                              return <Tooltip
                                key={action_id}
                                content={
                                  <Fragment>
                                    <p style={{ padding: "1em", fontWeight: "bold" }}>
                                      { actionTypes.getIn([action_id, "action_text"]) }
                                    </p>
                                    <ActionCostEffect actionId={action_id} unitCounts={unitCounts}/>
                                  </Fragment>
                                }
                                target={
                                  <Fragment>
                                    <Icon 
                                      icon={icon}
                                      iconSize={10}
                                      style={{position: "absolute", marginTop: "7px", marginLeft: "7px"}}
                                    />
                                    <Spinner className="pt-small" value={progresses.get(action_id)} />
                                  </Fragment>
                                }
                              />;
                            })
                        }
                      </td>
                    </tr>
                  );
                }).toArray()
              }
              <tr style={{ borderTop: "1px solid #ccc" }}>
                <th style={{ textAlign: "right", paddingTop: "1em" }}>
                  Sum
                </th>
                <td style={{ paddingTop: "1em" }}>
                  <UnitCountIcons noDivider counts={yledgerIntermediaries.get("upkeepSum")} />
                </td>
                <td style={{ paddingTop: "1em" }} colSpan="2">
                  <UnitCountIcons noDivider precision={1} counts={yledgerIntermediaries.get("workforceProductionCapacitySum")} />
                </td>
              </tr>
            </tbody>
          </table>
        </Panel>

        <Panel className="YLedgerPanel" title="Developer priorities" icon="mugshot">
          <table>
            <tbody>
              {
                allocationSlidersMeta.map(({ id, label }) => (
                  <AllocationSliderRow
                    key={id}
                    trigger={trigger}
                    appearedActions={appearedActions}
                    allocations={yledger.get("allocations")}
                    id={id}
                    label={label}
                    unitCounts={unitCounts}
                  />
                ))
              }
            </tbody>
          </table>
        </Panel>
      </Fragment>
    );
  }
}
