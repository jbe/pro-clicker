import { fromJS } from "immutable";
// import initialWorld from "data/initialWorld";

const patchers = {
  v0000: (world) => world
    .setIn(["actions", "managerAssignments"], fromJS({}))
    .setIn(["environmental_bonuses"], fromJS({}))
    .set("version", "v0001"),

  v0001: (world) => world
    .deleteIn(["actions", "appeared", "research_slaves"])
    .deleteIn(["actions", "appeared", "research_auto_finger"])
    .set("version", "v0002"),

  v0002: (world) => world
    .deleteIn(["actions", "appeared", "hire_marketer"])
    .set("version", "v0003"),

  v0003: (world) => world
    .deleteIn(["actions", "appeared", "disconnect"])
    .set("version", "v0004"),

  v0004: (world) => world
    .deleteIn(["actions", "appeared", "purge_manager"])
    .set("version", "v0005"),

  v0005: (world) => world
    .set("notificationLog", fromJS([]))
    .set("version", "v0006"),
};

export default function patchSave(world) {

  if (!world.get("version")) world = world.set("version", "v0000");

  while (patchers[world.get("version")])
  {
    console.log("Patching ", world.get("version"), "...");
    world = patchers[world.get("version")](world);
  }

  // world = initialWorld;
  // world = world.deleteIn(["flags", "win_screen_closed"]);
  // world = world.deleteIn(["actions", "appeared", "close"]);
  // world = world.setIn(["units", "counts", "loc"], 10000);
  // world = world.setIn(["units", "counts", "sp"], 1000);

  return world;
}