import React, { Component } from 'react';
import Panel from "ui/components/Panel";
import newsletters from "data/newsletters";


export default class NewsletterPage extends Component {

  render() {
    const {
      appearedNewsletters,
      notificationLog
    } = this.props;

    return (
      <div>
        <Panel width="25em">
          <div style={{maxHeight: "40em", overflowY: "auto" }}>
            <h3>Notification log</h3>
            {
              notificationLog.reverse().map((notification, i) => {
                if (notification.toJS) notification = notification.toJS();
                return (
                  <div key={i}>
                    <h5>{notification.header}</h5>
                    <p>{notification.text}</p>
                  </div>
                );
              })
            }
          </div>
        </Panel>
        {
          appearedNewsletters.map(newsletterId => {
            const newsletter = newsletters.get(newsletterId);

            return (
              <Panel width="25em">
                <div style={{fontFamily: 'Special Elite, cursive', fontSize: "1.1em"}}>
                  <img
                    style={{ maxWidth: "100%", maxHeight: "100%", height: "auto" }}
                    src={"/img/" + newsletter.get("image")}
                    alt={newsletter.get("header")}
                  />
                  <h5 style={{marginTop: "1em"}}>{newsletter.get("header")}</h5>
                  <div>
                    {newsletter.get("text").toJS()} (&hellip;)
                  </div>
                </div>
              </Panel>
            );
          })
        }
      </div>
    );
  }
}
