import React, {Component} from 'react';
import Panel from "ui/components/Panel";
import ActionTriggers from "action/components/ActionTriggers";

export default class ActionTriggerPanel extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  render() {
    const {
      header,
      icon,
      help,
      units,
      visibleActions,
      disabledActions,
      trigger,
      actionProduction
    } = this.props;

    if (!visibleActions.size) return null;

    const style = {
      width: "22rem",
      /* maxHeight: scrollable && "30em",
       * overflowY: scrollable && "auto",
       * paddingRight: scrollable && "1em"*/
    };

    return (
      <Panel className="ActionsPanel" title={header} icon={icon}>
        {help}
        <hr/>
        <div style={style}>
          <ActionTriggers
            units={units}
            visibleActions={visibleActions}
            disabledActions={disabledActions}
            trigger={trigger}
            actionProduction={actionProduction}
          />
        </div>
      </Panel>
    );
  }
}
