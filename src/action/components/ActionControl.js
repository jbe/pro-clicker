import React, {Component} from 'react';
import {Map} from "immutable";

import { Button, ButtonGroup, Tooltip } from "@blueprintjs/core";
import unitTypes from "data/unitTypes";
import actionTypes from "data/actionTypes";
import UnitCountTable from "unit/components/UnitCountTable";
import EffectTable from "effect/components/EffectTable";
import { hasMinUnits } from "sim/conditions";

import ProductionProgressBar from "action/components/ProductionProgressBar";
/* import UnitCount from "unit/components/UnitCount";*/

function ActionControlWrapper({children}) {
  return (
    <div style={{height: "3em"}}>
      {children}
    </div>
  );
}

export default class ActionControl extends Component {
  render() {
    const {
      actionId,
      trigger,
      counts,
      production,
      disabled,
      progressWidth,
      assignedManager
    } = this.props;

    const action = actionTypes.get(actionId);

    const {
      icon,
      action_text,
      time,
    } = action.toJS();

    const safeCost = action.get("cost") || Map();

    const enabled = !disabled && hasMinUnits(counts, safeCost) && !assignedManager;

    const buttonJsx = (
      production ?
        <ProductionProgressBar assignedManager={assignedManager} width={progressWidth} item={production}/>
        :
        (
          enabled
            ? <Button
                icon={icon}
                text={ action_text }
                onClick={() => trigger({
                    trigger_actions: [actionId]
                })}
              />
            : <span
                style={{color: assignedManager ? "#79b" : "grey", position: "relative", top: "0.5em", paddingRight: "0.5em"}}
              >
                { action_text }
              </span>
        )
    );

    const tipJsx = (
      <div style={{padding: "1em"}}>
        <p><strong>Cost:</strong></p>
        <UnitCountTable availableCounts={counts} counts={safeCost.set("time", time)}/>


        <p style={{marginTop: "2em"}}><strong>Effect:</strong></p>
        <EffectTable effect={action.get("effect")}/>
      </div>
    );

    const unitEffects = action.getIn(["effect", "units"]);
    const unitEffectId = unitEffects && unitEffects.keySeq().first();
    const unitProduced = unitTypes.get(unitEffectId);

    return <ActionControlWrapper>
      <ButtonGroup>
        <Tooltip
          content={tipJsx}
          target={<div>{buttonJsx}</div>}
        />
        {
          (!!unitProduced && unitProduced.get("category") === "workforce") && !!counts.get(unitEffectId) && (actionId !== "purge_manager") &&
            <Tooltip
              content={"Fire " + unitProduced.get("singular")}
              target={
                <Button
                  icon="delete"
                  onClick={() => trigger({
                    units: {
                      [unitEffectId]: -1
                    }
                  })}
                />
            }
            />
        }
      </ButtonGroup>
    </ActionControlWrapper>;
  }
}
