import React, { Component } from 'react';
import {
  ProgressBar,
  Tooltip
} from "@blueprintjs/core";
import { humanizeSecs } from "utils";
import actionTypes from "data/actionTypes";

export default class ProductionProgressBar extends Component {
  render() {
    const {
      item,
      width,
      assignedManager
    } = this.props;

    const {
      actionId,
      elapsed,
      total
    } = item.toJS();

    const action = actionTypes.get(actionId);

    const {
      action_text,
    } = action.toJS();

    const tipJsx = (
      <span>
        <strong>
          {action_text}
        </strong>
        <br/>
        {humanizeSecs(total - elapsed + 1)} remaining
      </span>
    );

    const indicatorJsx = (
      <div style={{marginTop: "0.4em"}}>
        {/* <p style={{display: "inline-block", marginRight: "1em"}}>
            <Icon title={action_text} icon={icon}/>
            </p> */}
        <div style={{display: "inline-block", width: width || "8em", marginRight: "1em"}}>
          <div style={{position: "absolute", marginTop: "-0.5em", color: assignedManager && "#79b"}}>
            {action_text}
          </div>
          <div style= {{marginTop: "1em"}}>
            <ProgressBar className="pt-no-stripes" value={elapsed/total} intent={assignedManager ? "primary" : "none"}/>
          </div>
        </div>
      </div>
    );

    return (
      <Tooltip
        position="top"
        content={tipJsx}
        target={indicatorJsx}
      />
    );
  }
}
