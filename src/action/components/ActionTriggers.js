import React, {Component} from 'react';
import ActionControl from "action/components/ActionControl";

export default class ActionTriggers extends Component {
  /* shouldComponentUpdate(nextProps, nextState) {
   *   return !this.props.units.equals(nextProps.units)
   *       || !this.props.category.equals(nextProps.category);
   * } */

  render() {
    const {
      units,
      visibleActions,
      disabledActions,
      trigger,
      actionProduction
    } = this.props;

    if (!visibleActions.size) return null;

    const itemsJsx = visibleActions.map((item, id) => {
      const {
        header,
        story_text
      } = item.toJS();

      return (
        <div key={id}>
          <h6>
            {header}
          </h6>
          <p style={{marginBottom: "2em"}}>
            {story_text}
          </p>
          <ActionControl
            actionId={id}
            disabled={disabledActions.hasIn([id])}
            counts={units.get("counts")}
            trigger={trigger}
            production={actionProduction.get(id)}
            progressWidth="20em"
          />
          <hr/>
        </div>
      );
    }).toArray();

    return (
      <div>
        {itemsJsx}
      </div>
    );
  }
}
